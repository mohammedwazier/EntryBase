import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

// import EndorseController from '../Src/Controller/EndorseController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3007

App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))

/*Route Produk*/
// App.get('/', (req, res) => {
// 	res.send(true);
// })
// App.post('/Homepage', VerifyMiddleware, ProdukController.HomepageList);
// App.post('/CreateVideoProduct', ProdukController.upload.fields([{name: 'video', maxCount: 1}, {name: 'thumbnail', maxCount: 5}]), ProdukController.UploadVideoProduct)
// App.post('/CreateVideoProduct',
// 	upload.array('video', 4),
// 	(req, res) => {
// 		console.log('hehehe', req.body, req.query, req.params)
// 		console.log(req.file)
// 		// return res.send(req,file)
// 		res.send(true)
// 	});

// App.post('/UploadVideo', )
/*End Route Produk*/

Server.listen(PORT, () => {
	console.log(`[${ENV}] Endorse on port ${PORT}`)
})