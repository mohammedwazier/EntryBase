import 'module-alias/register';

import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express';
import proxy from 'express-http-proxy';
import Http from 'http';
import { json, urlencoded } from 'body-parser';
import Compression from 'compression';
import Cors from 'cors';
import Helmet from 'helmet';
import Socket from 'socket.io';
import Colors from 'colors';
// import requestid from 'express-request-id';
import Path from 'path';

import LoggingAccess from '../Src/Middleware/LoggingAccess';

import { ErrorHandler } from '../Src/Middleware/ErrorHandler';

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require(`../../Swagger/swagger.${ENV}.json`);

const App = Express();
const Server = Http.createServer(App);

const PORT = MAINPORT;
const Host = 'localhost';

const IO = Socket(Server);
const WS = require('./WebSocket')(IO);

App.use(Compression());
App.use(Cors());
App.use(Helmet());
App.use(json());
App.use(urlencoded({extended: true}));

App.use(LoggingAccess) /*Logging All Access*/

App.use('/files', Express.static(Path.join(__dirname, '../../Source')));
App.use('/asset/image', Express.static(Path.join(__dirname, '../../Assets/Image')));

App.get('/', (req, res) => {
	res.send({
		projectname: NAME,
		description: 'What are you doing here, this is rectriction pages of EntryBase Microservices',
		version: VERSION,
	})
})

const SwitchingProxy = (port) => {
	return (req, res, next) => {
		let preserveHostHdr = true;
		let reqAsBuffer = false;
    	let reqBodyEncoding = true;
    	let parseReqBody = true;

	    let contentTypeHeader = req.headers['content-type'];
		if(req.headers['content-type'] && req.headers['content-type'].includes('multipart')){
			reqAsBuffer = true;
      		reqBodyEncoding = null;
      		parseReqBody = false;
		}

		return proxy(`${Host}:${port}${req.path}`, {
			proxyErrorHandler: ErrorHandler,
			preserveHostHdr,
	      	parseReqBody
		})(req, res, next);
	}

}

/*Proxy Forward to Service*/
App.use('/api/v1/Login', SwitchingProxy(3001));
App.use('/api/v1/Register', SwitchingProxy(3002));
App.use('/api/v1/Upload', SwitchingProxy(3003));
App.use('/api/v1/Profile', SwitchingProxy(3004));
App.use('/api/v1/Video', SwitchingProxy(3005));
App.use('/api/v1/Produk', SwitchingProxy(3006));
App.use('/api/v1/Endorse', SwitchingProxy(3007));
App.use('/api/v1/Utilization', SwitchingProxy(3008));
App.use('/api/v1/Following', SwitchingProxy(3009));
App.use('/view', SwitchingProxy(3010))
/*End Proxy Forward*/

/*Swagger API Documentation*/
App.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
/*End Swagger*/

App.use((req, res, next) => {
	res.status(404).send({
		state: false,
		code: 404,
		message: `Route '${req.originalUrl}' is Not Found, Please Check Documentation`,
		Documentation: '/api-docs'
	})
})

App.on('uncaughtException', (err) => {
    console.error(Colors.red(err.stack));
    process.exit(2);
});

Server.listen(PORT, () => {
	console.log(`[${ENV}] Main Server Running on Port ${PORT}`);
})

/*Handle Error*/
process.on("uncaughtException", e => {
    console.log(e);
    process.exit(1);
});
process.on("unhandledRejection", e => {
    console.log(e);
    process.exit(1);
});

/*Bundling to Module*/
module.exports = App;