import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

import LoginController from '../Src/Controller/LoginController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3001

App.use(Timeout('60s'))
App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))


/*Route Login*/
App.post('/Login', LoginController.LoginValidate)
App.post('/RequestForgotPassword', LoginController.RequestForgotPassword) /*only User*/
App.post('/ConfirmOTPRequestForgotPassword', LoginController.ConfirmRequestPassword) /*Only User*/
App.post('/ChangeForgotPassword', LoginController.ChangeForgotPassword) /*Only User*/
/*End Route Login*/

App.use((req, res, next) => {
	res.status(404).send({
		state: false,
		code: 404,
		message: `Route '${req.originalUrl}' is Not Found, Please Check Documentation`,
		Documentation: '/api-docs'
	})
})

Server.listen(PORT, () => {
	console.log(`[${ENV}] Login on port ${PORT}`)
})