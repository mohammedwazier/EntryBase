import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'
// import Multer from 'multer';

import ProdukController from '../Src/Controller/ProdukController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3006

// const upload = Multer()

App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))

/*Route Produk*/
App.post('/Homepage', VerifyMiddleware, ProdukController.HomepageList);
App.post('/CreateVideoProduct', ProdukController.upload.fields([{name: 'video', maxCount: 1}, {name: 'thumbnail', maxCount: 5}]), ProdukController.UploadVideoProduct)
// App.post('/UpdateProduct', VerifyMiddleware, ProdukController.UpdateProduk);
// App.post('/DeleteProduct', VerifyMiddleware, ProdukController.DeleteProduct);

// App.post('/UploadVideo', )
/*End Route Produk*/

Server.listen(PORT, () => {
	console.log(`[${ENV}] Produk on port ${PORT}`)
})