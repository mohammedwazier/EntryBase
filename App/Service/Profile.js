import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

import ProfileController from '../Src/Controller/ProfileController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3004

App.use(Timeout('60s'))
App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))

/*Route Login*/
// App.get('/', (req, res) => {
// 	res.send(true);
// })
App.post('/CheckProfile', VerifyMiddleware, ProfileController.CheckProfile)
App.post('/GetProfile', VerifyMiddleware, ProfileController.GetProfile)
App.post('/Upgrade', VerifyMiddleware, ProfileController.UpgradeProfile)
/*End Route Login*/

Server.listen(PORT, () => {
	console.log(`[${ENV}] Profile on port ${PORT}`)
})