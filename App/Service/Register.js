import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

import RegisterController from '../Src/Controller/RegisterController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3002

App.use(Timeout('60s'))
App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))

/*Route Register*/
App.post('/Validate', RegisterController.ValidateRegister)
App.post('/Register', RegisterController.RegisterUser)
// App.post('/BussinessRegister', VerifyMiddleware ,RegisterController.UpgradeBussiness)
// App.post('/TalentRegister', RegisterController.TalentRegister);
// App.post('/ValidateRegistration', RegisterController.ValidateRegistration)
// App.get('/vr/click/:verification', RegisterController.VerificationRegister)
// App.post('/RequestOTP', RegisterController.RequestOTP)
/*End Route Register*/

Server.listen(PORT, () => {
	console.log(`[${ENV}] Register on port ${PORT}`)
})