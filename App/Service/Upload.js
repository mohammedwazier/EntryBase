import { MAINPORT, VERSION, NAME, STATUS, ENV} from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import Busboy from 'connect-busboy';
import Compression from 'compression'
import Helmet from 'helmet'
import Multer from 'multer'
import Path from 'path';
import { json, urlencoded } from 'body-parser'

import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';
import UploadController from '../Src/Controller/UploadController';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3003

App.use(Compression())
App.use(Helmet())
App.use(urlencoded({extended: true}))


App.get('/', (req, res) => {
	res.send({
		page: 'Upload',
		state: true
	});
})

App.post('/UploadFile', UploadController.UploadingFilesV1)
App.post('/UploadFileV2', UploadController.upload.single('file'), UploadController.UploadingFilesV2)

Server.listen(PORT, () => {
	console.log(`[${ENV}] Upload on port ${PORT}`)
})