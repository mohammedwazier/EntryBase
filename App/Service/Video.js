import { MAINPORT, VERSION, NAME, STATUS, ENV } from '../Src/Config/Config';
if(!STATUS){
	console.log('Please fill the .env Variable like .env_example')
	process.exit();
}

import Express from 'express'
import Http from 'http'
import { json, urlencoded } from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

import VideoController from '../Src/Controller/VideoController'
import VerifyMiddleware from '../Src/Middleware/AuthMiddleware';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3005

// App.use(Timeout('60s')) /*Timeout*/
App.use(Compression())
App.use(Helmet())
App.use(json())
App.use(urlencoded({extended: true}))

App.get('/LoginVideo', VideoController.IndexVideo);
/*End Route Login*/

Server.listen(PORT, () => {
	console.log(`[${ENV}] Video on port ${PORT}`)
})