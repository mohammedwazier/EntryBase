import {
    MAINPORT,
    VERSION,
    NAME,
    STATUS,
    ENV
} from '../Src/Config/Config';
if (!STATUS) {
    console.log('Please fill the .env Variable like .env_example')
    process.exit();
}

import Express from 'express'
import Http from 'http'
import {
    json,
    urlencoded
} from 'body-parser'
import Compression from 'compression'
import Helmet from 'helmet'
import Timeout from 'connect-timeout'

import handlebars from 'express-handlebars';

import ViewPageController from '../Src/Controller/ViewPageController';

const App = Express()
const Server = Http.createServer(App)

const PORT = 3010

App.use(Timeout('60s'))
App.use(Compression())
App.use(Helmet())

const hbs = handlebars.create({
    extname: '.hbs',
    layoutsDir: '../../Template/',
})

App.engine('handlebars', hbs.engine);
App.set('view engine', 'handlebars');

/*Route View Page*/
App.get('/vr/click/:verification', ViewPageController.VerificationRegister)

Server.listen(PORT, () => console.log(`[${ENV}] View Page HTML on port ${PORT}`))