// import dotenv from 'dotenv';
require('dotenv').config();
const pkg = require('../../../package.json');

// dotenv.config();

let urlImage, urlData;
if(process.env.NODE_ENV === 'development'){
	urlImage = 'http://202.125.83.4:3000/files/'
	urlData = 'http://202.125.83.4:3000/'
}else if(process.env.NODE_ENV === 'local'){
	urlImage = 'http://202.125.83.4:3000/files/'
	urlData = 'http://202.125.83.4:3000/'
}else{
	/*Not Yet*/
}

const ResponseStructure = {
	data: undefined,
	code: 100,
	state: false,
	message: 'Get Data'
}

let statusEnv = true;

if(!process.env.ENV || !process.env.NODE_ENV){
	statusEnv = false
}

module.exports = {
	MAINPORT: process.env.MAINPORT || 3000,
	ENV: process.env.ENV || 'development',
	NODE_ENV: process.env.NODE_ENV || 'development',
	CIPHERID: process.env.CIPHERID,
	DATABASE: {
		uname: process.env.USERDB || 'root',
		passwd: process.env.PASSDB || '',
		host: process.env.HOSTDB || 'localhost',
		port: process.env.PORTDB || '3306',
		dialect: process.env.DIALECTDB || 'pg',
		url: process.env.DIALECTDB,
		db: process.env.DATABASE || 'profile'
	},
	NAME: pkg.name,
	VERSION: pkg.version,
	SLEEP: 750,
	URLIMAGE: urlImage,
	URLDATA: urlData,
	STRUCTURE: ResponseStructure,
	GEOLOCATION: process.env.GEOLOCATION,
	STATUS: statusEnv,
	EMAIL: {
		host: process.env.EMAILHOST,
		secure: process.env.EMAILSECURE,
		port: process.env.EMAILPORT,
		username: process.env.EMAILUSERNAME,
		password: process.env.EMAILPASSWORD
	}
}