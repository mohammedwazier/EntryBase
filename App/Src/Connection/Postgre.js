import knex from 'knex';
import knexConfig from './KnexFile';
import { NODE_ENV } from '../Config/Config';
let KNEX = knex(knexConfig[NODE_ENV]);

module.exports = KNEX;