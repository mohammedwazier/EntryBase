import MainController from './MainController';
import Database from '../Database/Database';

class LoginController extends MainController{
	constructor () {
		super();
	}

	LoginValidate = async (req, res) => {
		const { body } = req;
		let fields = ['input', 'password', 'type'];
		let newBody = Object.keys(body);
		let diff = fields.filter((x) => newBody.indexOf(x) === -1)
		try{
        	if(diff.length === 0){
        		const { input, password, type } = body;
        		let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        		ip = ip === '::ffff:127.0.0.1' ? '114.122.100.236' : ip;

                let loginData;

                if(type === 'user'){
                    loginData = await Database.profile_user.connection.raw(
                    `SELECT * FROM
                    profile_user
                    WHERE
                    pr_password = '${this.createPassword(password)}'
                    AND
                    (
                        LOWER(pr_username) LIKE LOWER('%${input.toLowerCase()}%')
                        OR
                        LOWER(pr_email) LIKE LOWER('%${input.toLowerCase()}%')
                        OR
                        CAST(pr_combinephone as VARCHAR) LIKE '%${input}%'
                    )
                    LIMIT 1
                    `)
                }else{
                    loginData = await Database.talent.connection.raw(
                    `SELECT * FROM
                    talent
                    WHERE
                    tl_password = '${this.createPassword(password)}'
                    AND
                    (
                        LOWER(tl_username) LIKE LOWER('%${input.toLowerCase()}%')
                        OR
                        LOWER(tl_email) LIKE LOWER('%${input.toLowerCase()}%')
                        OR
                        CAST(tl_combinephone as VARCHAR) LIKE '%${input}%'
                    )
                    LIMIT 1
                    `)
                }


                let data = loginData.rows;

        		if(data.length > 0){
        			let loginData = data[data.length - 1]

                    let headState = type === 'user' ? 'pr' : 'tl';

        			const profileData = {
        				id: loginData[`${headState}_id`],
        				nama: loginData[`${headState}_fullname`],
        				username: loginData[`${headState}_username`],
        				phonedata: {
        					countrycode: loginData[`${headState}_countrycode`],
        					number: loginData[`${headState}_numberphone`],
        					combine: loginData[`${headState}_combinephone`]
        				},
                        role: type
        			}
                    const Token = this.createToken(profileData);
                    profileData.token = Token.token;
                    let geolocation = await this.getLocation(ip);
                    geolocation = geolocation.data;
                    const insertData = {
                        log_profile_id: profileData.id,
                        log_token: Token.token,
                        log_status: 1,
                        log_ip: ip,
                        log_data: JSON.stringify(geolocation),
                        log_lat: geolocation.latitude,
                        log_ing: geolocation.longitude,
                        log_type: type
                    }
                    const insert = await Database.login.insertOne(insertData);
                    if(insert.state){
                        // client.set(input, JSON.stringify(data))
                    	res.send({
		        			state: true,
		        			code: 100,
		        			message: `Success Login, here's the credential to use Further API, Type : ${type}`,
		        			data: profileData
		        		})
                    }else{
                    	res.send({
		        			state: false,
		        			code: 103,
		        			message: `Failed to insert Login Data, Please try again soon, Type : ${type}`,
		        			data: body
		        		})
                    }
        		}else{
        			res.send({
	        			state: false,
	        			code: 102,
	        			message: `Account Not Found, please Check your Input and Try again, Type : ${type}`,
	        			data: body
	        		})
        		}
        	}else{
        		res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}', Type : ${type}`
        		})
        	}
        }catch(err){
        	console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'LoginController');
        	res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
        }
	}

    RequestForgotPassword = async (req, res) => {
        const { body } = req;
        let fields = [
         'input'
        ];
        let newBody = Object.keys(body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)

        try{
            if(diff.length === 0){
                const input = body.input.toLowerCase();
                let ProfileData = await Database.profile_user.connection.raw(`SELECT * FROM profile_user WHERE lower(pr_username) LIKE '%${input}%' OR lower(pr_email) LIKE '${input}' LIMIT 1`);
                if(ProfileData.rows.length > 0){
                    ProfileData = ProfileData.rows[0];
                    const OTP =  await this.getKodeOTP(ProfileData.pr_email, 'user');
                    let TextData = `Kode OTP Untuk Request Forgot Password adalah : ${OTP.kode}`;
                    const Email = await this.customSendingEmail('RequestNewPassword.hbs', {OTP: OTP.kode}, 'Request Forgot Password', TextData, ProfileData.pr_email);
                    if(Email){
                        return res.send({
                            state: true,
                            code: 100,
                            message: `Success send email to ${ProfileData.pr_email}, Please check Email to get OTP Request Forgot Password`,
                            data: {
                                email: ProfileData.pr_email,
                                input: body.input,
                                type: 'user',
                            }
                        })
                    }else{
                        return res.send({
                            state: false,
                            code: 103,
                            message: `Failed to Send Email to ${ProfileData.pr_email}`
                        })
                    }
                }else{
                    return res.send({
                        state: false,
                        code: 102,
                        message: `Account tidak ditemukan, ${body.input}`
                    })
                }

            }else{
                return res.send({
                    state: false,
                    code: 101,
                    message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
                })
            }
        }catch(err){
            console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'LoginController');
            return res.send({
                state: false,
                code: 500,
                message: 'Errors',
                errors: err.message
            })
        }
    }

    ConfirmRequestPassword = async (req, res) => { /*User Only*/
        const { body } = req;
        let fields = ['otp', 'input'];
        let newBody = Object.keys(body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)
        try{
            if(diff.length === 0){
                // Check OTP Code;
                // let OTPCheck = await Database.otp_list.allSelect({otp_output: body.input, otp_kode: body.kode});
                let OTPCheck = await Database.otp_list.connection.raw(`SELECT * FROM otp_list WHERE NOW() <= otp_expired AND otp_kode = '${body.otp}' LIMIT 1`);
                if(OTPCheck.rows.length > 0){
                    OTPCheck = OTPCheck.rows[0]
                    await Database.otp_list.updateOne({otp_id: OTPCheck.otp_id}, {otp_status: 5, otp_updated_at: this.createDate(0)})
                    return res.send({
                        state: true,
                        code: 100,
                        message: 'Kode OTP Valid, silahkan melanjutkan pergantian Password baru',
                        data: {
                            otp: body.otp,
                            email: body.input,
                            valid: 1
                        }
                    })
                }else{
                    return res.send({
                        state: false,
                        code: 102,
                        message: 'OTP Expired',

                    })
                }
            }else{
                return res.send({
                    state: false,
                    code: 101,
                    message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
                })
            }
        }catch(err){
            console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'LoginController');
            return res.send({
                state: false,
                code: 500,
                message: 'Errors',
                errors: err.message
            })
        }
    }

    ChangeForgotPassword = async (req, res) => {
        const { body } = req;
        let fields = ['email', 'newpassword', 'otp'];
        let newBody = Object.keys(body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)
        try{
            if(diff.length === 0){
                // Check OTP kode jika 5 berdasarkan Email dan Kode OTP
                // Ganti Password
                let OTP = await Database.otp_list.connection.raw(`SELECT * FROM otp_list WHERE otp_created_at >= NOW() - INTERVAL '24 HOURS' AND otp_output = '${body.email}' AND otp_kode = '${body.otp}' AND otp_status = 5 LIMIT 1`);
                if(OTP.rows.length > 0){
                    OTP = OTP.rows[0]
                    let ProfileUser = await Database.profile_user.allSelect({pr_email: body.email, pr_isactive: 1, pr_isdelete: 0, pr_issuspend: 0});
                    if(ProfileUser.length > 0){
                        ProfileUser = ProfileUser[0]
                        let updatePassword = await Database.profile_user.updateOne({pr_id: ProfileUser.pr_id}, {pr_password: this.createPassword(body.newpassword)});
                        if(updatePassword.state){
                            await Database.otp_list.updateOne({otp_id: OTP.otp_id}, {otp_status: 10});
                            return res.send({
                                state: true,
                                code: 100,
                                message: 'Change Password was success, please login with new Password',
                                data: {
                                    email: body.email
                                }
                            })
                        }else{
                            return res.send({
                                state: false,
                                code: 104,
                                message: 'Failed to Update new Password, please try again later',
                            })
                        }
                    }else{
                        return res.send({
                            state: false,
                            code: 103,
                            message: 'Akun tidak Ditemukan, Akun tidak Aktif',
                            data: {
                                email: body.email
                            }
                        })
                    }
                }else{
                    return res.send({
                        state: false,
                        code: 102,
                        message: 'OTP Tidak Ditemukan, Cek kembali inputan Email atau KODE OTP',
                    })
                }
            }else{
                return res.send({
                    state: false,
                    code: 101,
                    message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
                })
            }
        }catch(err){
            console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'LoginController');
            return res.send({
                state: false,
                code: 500,
                message: 'Errors',
                errors: err.message
            })
        }
    }
}

export default new LoginController