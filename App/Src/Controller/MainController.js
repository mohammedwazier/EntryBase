import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import 'moment';
import moment from 'moment-timezone';
import utf8 from 'utf8';
import { CIPHERID, GEOLOCATION, EMAIL, ENV } from '../Config/Config';
import API from '../Helper/API.js';
import Database from '../Database/Database.js';
import Nodemailer from 'nodemailer';
import SMTPTransport from 'nodemailer-smtp-transport';

import Mime from 'mime-types';

import Handlebars from 'handlebars';
import Path from 'path';
import FS from 'fs';

import Multer from 'multer';

moment.tz.setDefault('Asia/Jakarta');

export default class MainController {
	constructor(){
        this.env = ENV;
    }

    /*Multer Upload Main*/
    storage = Multer.diskStorage({
        destination: Path.join(__dirname + '../../../../Source/'),
        filename: (req, file, cb) => {
            let name= file.originalname.split('.')
            name[0] = name[0].replace(/\s/g,'');
            name[0] = name[0].replace(/[^0-9a-z]/gi, '');
            name = `${this.generateID()}_${name[0]}.${name[name.length-1]}`;
            Database.media.insertOne({
                md_id: this.generateID(),
                md_filename: name,
                md_realname: file.originalname,
                md_mime: file.mimetype,
                md_data: JSON.stringify(file),
                md_size: file.size,
                md_created_at: this.createDate(0),
                md_profileupload: req.body.id || null
            })
            cb(null, name)
        }
    })

    upload = Multer({
        storage: this.storage,
        limits: {
            fileSize: 999999999
        }
    })

    CreateProdukVideoUpload = Multer({
        storage: this.storage
    }).array('files', 10);

    test = () =>{
        console.log('ada harusnya')
    }

	romanize = (num) => {
      var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},roman = '',i;
        for ( i in lookup ) {
            while ( num >= lookup[i] ) {
              roman += i;
              num -= lookup[i];
            }
        }
        return roman;
    }

    commandJobs = () =>{
        return new Promise(resolve => {
            return resolve({state:true, data: null, sleep: 750})
        })
    }

    makeid = (length) => {
        var result           = '';
        var characters       = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    enc_base64_image = (filepath) => {
        var bitmap = FS.readFileSync(filepath);
        return new Buffer(bitmap).toString('base64');
    }

    readHTMLFile = (path, callback) => {
        FS.readFile(path, {encoding: 'utf-8'}, function(err, html){
            if(err){
                throw err;
                callback(err);
            }else{
                callback(null, html);
            }
        })
    }

    sendErrorDatabase = async (code = 0, message = null, data, func) => {
        await Database.error_logs.insertOne({
            err_code: code,
            err_message: message,
            err_data: data,
            err_func: func
        })
    }

    customSendingEmail = (template, DATA, subject, text, emailTo) => {
        return new Promise(async resolve => {
            try{
                const ID = this.generateID();

                let { host, port, secure, username, password } = EMAIL;
                let transporter;
                if(ENV === 'local' || ENV === 'development'){
                    host = 'gmail',
                    username = 'dummy.wazier@gmail.com'
                    password = 'qlikoodzsmrwekll'
                    transporter = Nodemailer.createTransport({
                        service: host,
                        auth: {
                            user: username,
                            pass: password
                        }
                    })

                }else{
                    // Production Environtment Email
                }

                const sourcePage = FS.readFileSync(Path.join(__dirname, `../../../views/${template}`), 'utf-8');
                let templatePage =  Handlebars.compile(sourcePage);

                let urlImage;
                if(ENV !== 'production'){
                    urlImage = 'http://202.125.83.4:3000/asset/image/EntryShow.png'
                }else{
                    /*Production*/
                }

                let tempEmail = {
                    from: '"Entry" <entry-noreply@gmail.com>',
                    to: emailTo,
                    replyTo: 'entry-noreply@gmail.com',
                    subject: subject,
                    text: text,
                    html: templatePage(DATA)
                }

                let insertEmail = await Database.email_send.insertOne({
                    email_id: ID,
                    email_from: username,
                    email_host: host,
                    email_status: 0,
                    email_to: emailTo,
                    email_html: tempEmail.html.toString(),
                    email_text: tempEmail.text.toString(),
                    email_jsonraw: JSON.stringify(tempEmail),
                    email_created_at: this.createDate(0)
                })


                if(insertEmail.state){
                    await transporter.verify(async function(error, success) {
                        if (error) {
                            console.log('error');
                            await Database.email_send.updateOne({email_id: ID}, {email_status: 3}) /*3 berarti emailnya gagal dikirim, error*/
                            resolve({
                                state: false,
                                code: 2
                            })
                        } else {
                            // console.log("Server is ready to take our messages");
                        }
                    });

                    let info = await transporter.sendMail(tempEmail);
                    await Database.email_send.updateOne({email_id: ID}, {email_status: 1, email_messageid: info.messageId})
                    resolve({
                        state: true,
                        code: 1
                    })
                }else{
                    resolve({
                        state: false,
                        code: 1
                    })
                }
            }catch(err){
                console.log(err);
                await this.sendErrorDatabase(0, err.message, err.stack, 'MainController');
                resolve({
                    state: false,
                    code: 0
                })
            }
        })
    }

    sendingEmail = (AccountData) => {
        // console.log(AccountData)
        // process.exit()
        return new Promise(async resolve => {
            try{
                // Create Email Data, with status
                const ID = this.generateID();

                let { host, port, secure, username, password } = EMAIL;
                let transporter;
                if(ENV === 'local' || ENV === 'development'){
                    host = 'gmail',
                    username = 'dummy.wazier@gmail.com'
                    password = 'qlikoodzsmrwekll'
                    transporter = Nodemailer.createTransport({
                        service: host,
                        auth: {
                            user: username,
                            pass: password
                        }
                    })
                }else{
                    // Production Environtment Email
                }


                const sourcePage = FS.readFileSync(Path.join(__dirname, '../../../views/OTPSendEmail.hbs'), 'utf-8');

                let template =  Handlebars.compile(sourcePage);

                let urlImage;
                if(ENV !== 'production'){
                    urlImage = 'http://202.125.83.4:3000/asset/image/EntryShow.png'
                }else{
                    /*Production*/
                }

                let tempEmail = {
                    from: '"Entry" <entry-noreply@gmail.com>',
                    to: AccountData.email,
                    replyTo: 'entry-noreply@gmail.com',
                    subject: 'Entry OTP Verification',
                    text: `To authenticate, please use the following One Time Password (OTP): ${AccountData.otp}.\nDo not share this OTP with anyone. Entry takes your account security very seriously. Entry Customer Service will never ask you to disclose or verify your Entry password, OTP, credit card, or banking account number. If you receive a suspicious email with a link to update your account information, do not click on the link--instead, report the email to Entry for investigation.\nWe hope to see you again soon.`,
                    html: template({OTP: AccountData.otp, URLIMAGE:urlImage})
                }

                let insertEmail = await Database.email_send.insertOne({
                    email_id: ID,
                    email_from: username,
                    email_host: host,
                    email_status: 0,
                    email_to: AccountData.email,
                    email_html: tempEmail.html.toString(),
                    email_text: tempEmail.text.toString(),
                    email_jsonraw: JSON.stringify(tempEmail),
                    email_created_at: this.createDate(0)
                })


                if(insertEmail.state){
                    await transporter.verify(async function(error, success) {
                        if (error) {
                            console.log('error');
                            await Database.email_send.updateOne({email_id: ID}, {email_status: 3}) /*3 berarti emailnya gagal dikirim, error*/
                            resolve(false)
                        } else {
                            console.log("Server is ready to take our messages");
                        }
                    });

                    let info = await transporter.sendMail(tempEmail);

                    // console.log("Message sent: %s", info.messageId);
                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

                    // Preview only available when sending through an Ethereal account
                    // console.log("Preview URL: %s", Nodemailer.getTestMessageUrl(info))
                    // await Database.email_send.updateOne({email_id: ID}, {email_status: 1})
                    await Database.email_send.updateOne({email_id: ID}, {email_status: 1, email_messageid: info.messageId})
                    resolve(true)
                }else{
                    // await Database.email_send.updateOne({email_id: ID}, {email_status: 2}) /*Failed to insert Email Send*/
                    resolve(false)
                }


            }catch(err){
                console.log(err);
                await this.sendErrorDatabase(0, err.message, err.stack, 'MainController');
                resolve(false)
            }
        })
    }

    renderTemplate = (filepath, value) => {
        return new Promise(async resolve => {
            try{
                const sourcePage = FS.readFileSync(Path.join(__dirname, `../../../views/${filepath}`), 'utf-8');
                let template = Handlebars.compile(sourcePage);
                resolve({
                    state: true,
                    html: template(value)
                })
            }catch(err){
                resolve({
                    state: false
                })
            }

        })
    }

    SendingActiveEmail = (FORM) => {
        return new Promise(async resolve => {
            try{
                // Create Email Data, with status
                const ID = this.generateID();

                let { host, port, secure, username, password } = EMAIL;
                let transporter;
                if(ENV === 'local' || ENV === 'development'){
                    host = 'gmail',
                    username = 'dummy.wazier@gmail.com'
                    password = 'qlikoodzsmrwekll'
                    transporter = Nodemailer.createTransport({
                        service: host,
                        auth: {
                            user: username,
                            pass: password
                        }
                    })
                }else{
                    // Production Environtment Email
                }

                const sourcePage = FS.readFileSync(Path.join(__dirname, '../../../views/SuccessVerification.hbs'), 'utf-8');

                let template =  Handlebars.compile(sourcePage);

                let urlImage;
                if(ENV !== 'production'){
                    urlImage = 'http://202.125.83.4:3000/asset/image/EntryShow.png'
                }else{
                    /*Production*/
                }

                FORM.URLIMAGE = urlImage

                let tempEmail = {
                    from: '"Entry" <entry-noreply@gmail.com>',
                    to: FORM.EMAIL,
                    replyTo: 'entry-noreply@gmail.com',
                    subject: 'Success Activate Entry Account',
                    text: `Success Activate Account, Please login to User Entry Application`,
                    html: template(FORM)
                }

                let insertEmail = await Database.email_send.insertOne({
                    email_id: ID,
                    email_from: username,
                    email_host: host,
                    email_status: 0,
                    email_to: FORM.EMAIL,
                    email_html: tempEmail.html,
                    email_text: tempEmail.text,
                    email_jsonraw: JSON.stringify(tempEmail),
                    email_created_at: this.createDate(0)
                })

                if(insertEmail.state){
                    await transporter.verify(async function(error, success) {
                        if (error) {
                            console.log('error');
                            await Database.email_send.updateOne({email_id: ID}, {email_status: 3}) /*3 berarti emailnya gagal dikirim, error*/
                            resolve(false)
                        } else {
                            console.log("Server is ready to take our messages");
                        }
                    });

                    let info = await transporter.sendMail(tempEmail);

                    // await Database

                    // console.log("Message sent: %s", info.messageId);
                    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

                    // Preview only available when sending through an Ethereal account
                    // console.log("Preview URL: %s", Nodemailer.getTestMessageUrl(info))
                    await Database.email_send.updateOne({email_id: ID}, {email_status: 1, email_messageid: info.messageId})
                    resolve(true)
                }else{
                    // await Database.email_send.updateOne({email_id: ID}, {email_status: 2}) /*Failed to insert Email Send*/
                    resolve(false)
                }


            }catch(err){
                console.log(err);
                await this.sendErrorDatabase(0, err.message, err.stack, 'MainController');
                resolve(false)
            }
        })
    }

    ValidateRouteFields = (fields, body) => {
    	return new Promise(async (resolve) => {
            let response = STRUCTURE;
            let newBody = Object.keys(body);
            let diff = list.filter((x) => newBody.indexOf(x) === -1)
            try{
            	if(diff.length === 0){
            		resolve({
            			state: true,
            			code: 100,
            			message: 'Fields are Valid',
            			data: body
            		})
            	}else{
            		resolve({
            			state: false,
            			code: 101,
            			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`,
            			data: diff
            		})
            	}
            }catch(err){
                await this.sendErrorDatabase(0, err.message, err.stack, 'MainController');
            	resolve({
            		state: false,
            		code: 102,
            		message: 'Something Error on Validating the Fields on Route'
            	})
            }
        });
    }

    createDate = (add = 24, type= 'hours') => {
        if(['hours', 'days', 'months', 'years', 'minutes', 'secods'].includes(type)){
            let date = moment().tz("Asia/Jakarta").format("YYYY-MM-DDTHH:mm:ssZ")
            date = moment(date).add(add, type).format('YYYY-MM-DDTHH:mm:ssZ');
            return date;
        }else{
            return null;
        }
    }

    createDateFormat = (date, format = 'LLLL') => {
        return moment(date).format(format);
    }

    random = (start= 1, end= 9) => {
        const val = Math.floor(start + Math.random() * end)
        return val
    }

    validateDate = (date) =>{
        let retDate = date < 10 ? `0${date}` : date
        return retDate
    }

    milli = () => {
        const hrTime = process.hrtime()
        return hrTime[0] * 1000 + hrTime[1] / 1000000
    }

    generateID = () => {
        let date = new Date()
        let year = date.getFullYear()
        let month = date.getMonth() + 1
        month = this.validateDate(month)
        let day = date.getDay() + 1
        day = this.validateDate(day)

        let hour = date.getHours()
        hour = this.validateDate(hour)
        let minutes = date.getMinutes()
        minutes = this.validateDate(minutes)
        let second = date.getSeconds()
        second = this.validateDate(second)

        let milisec = date.getMilliseconds()
        if (milisec < 10) {
            milisec = `${this.random()}${this.random()}${milisec}`
        } else if (milisec < 100) {
            milisec = `${this.random()}${milisec}`
        }

        let lastmil = this.milli()
        lastmil = lastmil.toString()
        if (parseInt(lastmil.length) === 15) {
            lastmil = parseInt(lastmil.substr(lastmil.length - 6))
            if (Number.isNaN(lastmil)) {
            lastmil = this.random(100000, 999999)
            }
        } else {
            lastmil = this.random(100000, 999999)
        }
        let id = `${year}${month}${day}${hour}${minutes}${second}${milisec}${lastmil}`
        id = id.length <= 21 ? `${id}${this.random(10, 99)}` : id
        id = id.length >= 22 ? id.substr(0, 20) : id
        return id
    }

    createPassword = (password) => {
        let buff = Buffer.from(password)
        let base64Data = buff.toString('base64')
        base64Data = utf8.encode(base64Data);

        const secretsha1 = CIPHERID;
        let dataSha1 = crypto
            .createHmac('sha1', secretsha1)
            .update(base64Data)
            .digest('hex')

        return dataSha1
    }

    convertToRupiah = (angka) =>{
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
    }

    getLocation = (ipAddress = '192.1.1.1') => {
        return new Promise(async (resolve) => {
            let url = `http://api.ipstack.com/${ipAddress}?access_key=${GEOLOCATION}`
            let result = await API.get(url);
            resolve(result);
        })
    }

    createToken = (data) => {
        const token = jwt.sign(data, CIPHERID, {expiresIn: 0x31536000});
        return {auth: true, token: token};
    }

    generateOTP = () => {
        let otp = this.random(1000000000, 9999999999);
        if(otp.toString().length > 4){
            otp = otp.toString();
            otp = otp.substr(0, 3);
            otp = Number(otp);
        }
        return otp;
    }

    getKodeOTP = (input, type) => {
        return new Promise(async (resolve) => {
            try{
                const output = input;
                let kode = this.generateOTP();
                const ID = this.generateID();
                if(kode.length > 10){
                    kode = kode.substr(0, 9);
                }

                let OTPVerif = await Database.otp_list.allSelect({otp_kode: kode, otp_status: 0, otp_type: type});
                if(OTPVerif.length > 0){
                    this.getKodeOTP()
                }

                const otp_listStructure = {
                    otp_id: ID,
                    otp_output: output,
                    otp_kode: kode,
                    otp_status: 0,
                    otp_type: type,
                    otp_created_at: this.createDate(0),
                    otp_expired: this.createDate(60, 'minutes')
                }
                const result = await Database.otp_list.insertOne(otp_listStructure);
                result.kode = kode;
                resolve(result);
            }catch(err){
                console.log(err)
                // SendErrorController.message(`Error on MainController\n${JSON.stringify(err.stack)}`)
                resolve({state: false, message: err.stack})
            }
        })

    }
}
// module.exports = new MainController