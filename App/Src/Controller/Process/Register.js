import MainController from '../MainController';
import Database from '../../Database/Database';
import crypto from 'crypto';

export default class Register extends MainController{
    constructor(args){
        super();
        this.args = args;
        this.username = args.username
        this.passwordRaw = args.passwordRaw
        this.email = args.email
        this.fullname = args.fullname
        this.countrycode = args.countrycode
        this.numberphone = args.numberphone
        this.combinephone = args.combinephone
        this.type = args.type
        this.id = this.generateID()
        this.state = this.validateArgs()
    }

    validateArgs = () => {
        let b = true;
        if(!this.username || !this.passwordRaw || !this.email || !this.fullname){
            b = false;
        }
        return b;
    }

    getStatusFill = () => {
        return this.state;
    }

    getData(){
        return this.args
    }

    registerUser = async () => {
        let profileRegister = {
            pr_id: this.id,
            pr_username: this.username,
            pr_password: this.createPassword(this.passwordRaw),
            pr_email: this.email,
            pr_countrycode: this.countrycode,
            pr_numberphone: this.numberphone,
            pr_combinephone: this.combinephone,
            pr_fullname: this.fullname,
            pr_role: 'user',
            pr_statusrole: 'user',
            pr_isactive: 0,
            pr_created: this.createDate(0)
        }
        profileRegister.pr_registrationhash = crypto.createHash('sha256').update(JSON.stringify(profileRegister)).digest('hex').toString()
        const insertData = await Database.profile_user.insertOne(profileRegister)
        return {
            ...insertData,
            profileid: profileRegister.pr_id,
            encrypted: profileRegister.pr_registrationhash,
            fullname: profileRegister.pr_fullname,
            email: profileRegister.pr_email
        }

        /* Unused Variables */
        /* let profileRegister = {
            pr_id: this.generateID(),
            pr_username: body.username,
            pr_password: this.createPassword(body.password),
            pr_email: body.email,
            pr_countrycode: body.countrycode,
            pr_numberphone: body.numberphone,
            pr_combinephone: body.combinephone,
            pr_fullname: body.fullname,
            pr_role: 'user',
            pr_statusrole: 'user',
            pr_isactive: 0,
            // pr_address: body.address,
            // pr_province: body.province,
            // pr_city: body.city,
            // pr_district: body.district,
            // pr_subdistrict: body.subdistrict,
            // pr_village: body.village,
            // pr_zipcode: body.zipcode
        }
        */
    }

    registerTalent = () => {

    }

    
}