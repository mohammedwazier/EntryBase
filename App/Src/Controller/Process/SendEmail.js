export default class SendEmail {
    constructor(args){
        this.args = args;
        this.to = args.to
        this.subject = args.subject

        this.state = this.validateInput()
    }

    validateInput = () => {
        let b = true;
        if(!this.to || !this.subject){
            b = false
        }
        return b;
    }

    setLayout = (file) => {
        this.layout = file;
    }
    
}