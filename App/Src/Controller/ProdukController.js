import MainController from './MainController';
import Database from '../Database/Database';
import Multer from 'multer';
import Path from 'path';

class ProfileController extends MainController {
	constructor(){
		super();
	}

	HomepageList = async (req, res) => {
		const { body, query, params } = req;
		let fields = ['id'];
		let newBody = Object.keys(body);
		let diff = fields.filter((x) => newBody.indexOf(x) === -1)
		try{
			if(diff.length === 0){
				let FollowingListData = await Database.following.allSelect({fl_userid: body.id});
				console.log(FollowingListData)
				if(FollowingListData.length > 0){
					let list = FollowingListData.map(d => `'${d.fl_talentid}'`).join();
					// Get Video frol Following
					let query = `
					SELECT DISTINCT ON (prd.prod_talentid)
					prd.*
					FROM product as prd
					WHERE prd.prod_talentid IN (${list})
					ORDER BY prd.prod_talentid DESC
					LIMIT 10
					`
					let Data = await Database.product.connection.raw(query);
					return res.send({
						state: true,
						code: 100,
						data: Data.rows,
						message: 'Success get Homepage List Video, Please follow Them to get another Info'
					})

				}else{
					// Ambil data video tiap produk
					let query = `
					SELECT DISTINCT ON (prd.prod_talentid)
					prd.*
					FROM product as prd
					ORDER BY prd.prod_talentid DESC
					LIMIT 10
					`
					let Data = await Database.product.connection.raw(query);
					return res.send({
						state: true,
						code: 100,
						data: Data.rows,
						message: 'Success get Homepage List Video, Please follow Them to get another Info'
					})
				}
			}else{
				return res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
			}
		}catch(err){
			console.log(err)
			await this.sendErrorDatabase(0, err.message, err.stack, 'ProdukController');
        	return res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}

	UploadVideoProduct = async (req, res, next) => {
		try{
			const file = {...req.files}
			let body = {...req.body};
			Object.keys(file).map(data => {
				file[data].map(d => {
					body[d.fieldname] = d.filename
					body[`${d.fieldname}_detail`] = d
				})
			})

			let fields = [
				'id',
				'judul',
				'description',
				'video',
				'thumbnail'
			];

			let newBody = Object.keys(body);
			let diff = fields.filter((x) => newBody.indexOf(x) === -1)
			if(diff.length === 0){
				// Likes Comment
				let insertProduk = {
					prod_id: this.generateID(),
					prod_code: this.makeid(10),
					prod_title: body.judul,
					prod_name: body.judul,
					prod_desc: body.description,
					prod_video: body.video,
					prod_imgthumb: body.thumbnail,
					prod_created: this.createDate(0),
					prod_isbuy: 0,
					prod_cost: 0,
					prod_type: 'testing',
					prod_data: JSON.stringify(body),
					prod_talentid: body.id
				}
				let insert = await Database.product.insertOne(insertProduk);
				if(insert.state){
					return res.send({
						state: true,
						code: 100,
						message: `Success Create new Video ${body.judul}`
					})
				}else{
					res.send({
	        			state: false,
	        			code: 102,
	        			message: `Failed to create Product, please try Again`,
	        			data: body
	        		})
				}
			}else{
				res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
			}
		}catch(err){
			console.log(err)
			await this.sendErrorDatabase(0, err.message, err.stack, 'ProdukController');
        	return res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}

	UpdateProduk = (req, res) => {
		let fields = [
				'id',
				'judul',
				'description',
				'thumbnail',
				'status'
			];
		// res.send(true)
	}

}

module.exports = new ProfileController;