import MainController from './MainController';
import Database from '../Database/Database';

class ProfileController extends MainController {
	constructor(){
		super();
	}

	CheckProfile = async (req, res) => {
		const { body } = req;
		let fields = ['id'];
		let newBody = Object.keys(body);
		let diff = fields.filter((x) => newBody.indexOf(x) === -1)
		try{
			if(diff.length === 0){
				let userProfile = await Database.profile_user.allSelect({pr_id: body.id, pr_isactive: 1})
				if(userProfile.length > 0){
					userProfile = userProfile[userProfile.length - 1];
					let statusRole = 'user';
					if(userProfile.pr_role === 'user' && userProfile.pr_statusrole === 'user' && userProfile.pr_upgrade === 0){
						statusRole = 'user';
					}else if(userProfile.pr_role === 'user' && userProfile.pr_statusrole === 'bussiness' && userProfile.pr_upgrade === 1){
						statusRole = 'bussiness'
					}else if(userProfile.pr_role === 'admin' && userProfile.pr_statusrole === 'admin'){
						statusRole = 'admin';
					}

					res.send({
						state: true,
						code: 1000,
						message: 'Get Profile Checked',
						data: {
							role: statusRole,
							id: userProfile.pr_id,
							nama: userProfile.pr_fullname,
							username: userProfile.pr_username
						}
					})
				}else{
					res.send({
	        			state: false,
	        			code: 102,
	        			message: `Account Not Found, please Check your Input and Try again`,
	        			data: body
	        		})
				}
			}else{
				res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
			}
		}catch(err){
			console.log(err)
			await this.sendErrorDatabase(0, err.message, err.stack, 'ProfileController');
        	res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}

	GetProfile = async (req, res) => {
		const { body } = req;
		let fields = ['id', 'type'];
		let newBody = Object.keys(body);
		let diff = fields.filter((x) => newBody.indexOf(x) === -1)
		try{
			if(diff.length === 0){
				const { id, type } = body;
				let profileUser;
				if(type === 'user'){
					profileUser = await Database.profile_user.connection.raw(`
						SELECT
						*
						FROM profile_user
						WHERE
						pr_id = '${id}'
						AND
						pr_isactive = 1
						LIMIT 1
					`)
				}else if(type === 'talent'){
					profileUser = await Database.talent.connection.raw(`
						SELECT
						*
						FROM talent
						WHERE
						tl_id = '${id}'
						AND
						tl_isactive = 1
						LIMIT 1
					`)
				}

				profileUser = profileUser.rows

				if(profileUser.length > 0){
					profileUser = profileUser[0];
					res.send({
						state: true,
						code: 100,
						message: `Account Found, Type : ${type}`,
						data: profileUser
					})
				}else{
					res.send({
						state: false,
						code: 102,
						message: `Account not Found, Type : ${type}`
					})
				}
			}else{
				res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
			}
		}catch(err){
			console.log(err)
			await this.sendErrorDatabase(0, err.message, err.stack, 'ProfileController');
        	res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}

	UpgradeProfile = async (req, res) => {
		return res.send(true)
		const { body } = req;
		let fields = [
			'ktp_name',
			'ktp_address',
			'ktp_number',
			'ktp_photo',
			'id'
		];

		let newBody = Object.keys(body);
		let diff = fields.filter((x) => newBody.indexOf(x) === -1)
		try{
			if(diff.length === 0){

			}else{
				res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
			}
		}catch(err){
			console.log(err)
			await this.sendErrorDatabase(0, err.message, err.stack, 'ProfileController');
        	res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}
}

module.exports = new ProfileController;