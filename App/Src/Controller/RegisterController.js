import MainController from './MainController';
import Database from '../Database/Database';

import Register from './Process/Register';

class RegisterController extends MainController{
	constructor () {
		super();
	}
	ValidateRegister = async (req, res, next) => {
		try{
			let fields = [
				'fullname',
				'username',
				'email',
				'phone'
			];

			const { type, value, jenis } = req.body;

			if(jenis.length === 0){
				return res.send({state: false, code: 101, message: `Masukkan jenis Akun yang akan di Validate [user, talent]`})
			}

			if(Array.isArray(type) && Array.isArray(value)){
				if(type.length !== value.length){
					res.send({state: false, code: 103, message: 'Type and Value not Sync'})
				}else{
					let objData = [];
					let maxLength = Math.min(type.length, value.length);

					let head = jenis === 'user' ? 'pr' : 'tl';

					for(let idx = 0; idx < maxLength; idx++){
						if(type[idx] === 'phone'){
							objData.push({
								type: `${head}_combinephone`,
								value: value[idx]
							})
						}else{
							if(fields.includes(type[idx])){
								objData.push({
									type: `pr_${type[idx]}`,
									value: value[idx]
								})
							}
						}
					}
					Promise.all(objData.map(async (d, key) => {
						let response;
						if(jenis === 'user'){
							response = await Database.profile_user.allSelect({[d.type]: d.value});
						}else if(jenis === 'talent'){
							response = await Data.talent.allSelect({[d.type]: d.value});
						}
						let data = {
							type: type[key],
							state: response.length === 0 ? true : false,
							code: response.length === 0 ? 100 : 102,
							message: response.length === 0 ? `'${type[key]}' : '${d.value}' Valid untuk register` : `'${type[key]}' : '${d.value}' tidak Valid untuk register, Jenis Profile : ${jenis}`
						}
						return data;
					}))
					.then(response => {
						return res.send({
							state: true, code: 100, data: response
						})
					})
				}
			}else{
				let head = jenis === 'user' ? 'pr' : 'tl';
				switch(type){
					case  'fullname' :
					case 'username':
					case 'email':
					case 'phone': {
						let where = {}
						if(type === 'phone'){
							where[`${head}_combinephone`] = value
						}else{
							where[`${head}_${type}`] = value
						}
						let data;
						if(jenis === 'user'){
							data = await Database.profile_user.allSelect(where)
						}else if(jenis === 'talent'){
							data = await Database.talent.allSelect(where)
						}
						if(data.length === 0){
							return res.send({state: true, code: 100, message: `'${type}' : '${value}' valid untuk register, Jenis Profile : ${jenis}`, data: req.body})
						}else{
							return res.send({state: true, code: 102, message: `${type} : ${value} sudah diambil, Jenis Profile : ${jenis}`, data: req.body})
						}
					} break;
					default: {
						return res.send({state: false, code: 101, message: `Type tidak ditemukan, mohon mencoba dengan type '${fields.toString()}', Jenis Profile : ${jenis}`})
					} break;
				}
			}
		}catch(err){
            await this.sendErrorDatabase(0, err.message, err.stack, 'RegisterController');
			console.log(err)
        	return res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
		}
	}

	RegisterUser = async (req, res) => {
		let fields = ['username', 'password', 'email', 'countrycode', 'numberphone', 'combinephone', 'fullname'];
		/*, 'address', 'province', 'city', 'district', 'subdistrict', 'village', 'zipcode'*/ //Unused
		let newBody = Object.keys(req.body);
                let diff = fields.filter((x) => newBody.indexOf(x) === -1)
                try{
                	const { body } = req;
                	if(diff.length === 0){
                		/*
                		Mandatory Register 26 June 2020
                		Username, Email, Password, Nohp, Nama Lengkap
						*/
						let register = new Register({
							...body,
							passwordRaw: body.password
						});
						let userData = await register.registerUser();
						if (userData.state) {
							let templateEmail = await Database.setting.single({
								st_kode: 't_register'
							});
							let urlVerif = 'http://localhost:3000';
							if(this.env === 'development'){
								urlVerif = "http://http://202.125.83.4:3000";
							}else if(this.env === 'production'){

							}

							const Email = await this.customSendingEmail(templateEmail.st_value, {
								fullname: userData.fullname,
								fullurl: `${urlVerif}/view/vr/click/${userData.encrypted}`,
								created: this.createDate(0)
							}, 'Verifikasi Akun Entry', '', userData.email)
							if(Email.state){
								return res.send({
									state: true,
									code: 100,
									message: 'Success register Akun, please check email for Account Verification.'
								})
							}else{
								return res.send({
									state: false,
									code: 103,
									message: 'Error Sending Email, email was stored in Database, please contact Administrator.'
								})
							}
						}else{
							return res.send({
								state: false,
								code: 102,
								message: 'Error insert Register User Profile, please try again, or contact Administrator.'
							})
						}
                	} else{
						res.send({
							state: false,
							code: 101,
							message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
						})
					}
                }catch(err){
                    await this.sendErrorDatabase(0, err.message, err.stack, 'RegisterController');
                	console.log(err)
                	res.send({
                		state: false,
                		code: 500,
                		message: 'Errors',
                		errors: err.message
                	})
                }
	}

	UpgradeBussiness = (req, res, next) => {
		/*Step 1, Validasi*/
		res.send(true)
	}

	TalentRegister = async (req, res) => {
		let fields = ['username', 'password', 'email', 'countrycode', 'numberphone', 'combinephone', 'address', 'fullname', 'placebirth', 'datebirth', 'bio', 'photo', 'desc'];
		/* 'province', 'city', 'district', 'subdistrict', 'village', 'zipcode'*/ //Unused
		let newBody = Object.keys(req.body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)
        try{
        	const { body } = req;
        	if(diff.length === 0){
        		const talentID = this.generateID()
        		let talentRegister = {
        			tl_id: talentID,
        			tl_username: body.username,
        			tl_password: this.createPassword(body.password),
        			tl_email: body.email,
        			tl_countrycode: body.countrycode,
        			tl_numberphone: body.numberphone,
        			tl_combinephone: body.combinephone,
        			tl_fullname: body.fullname,
        			tl_address: body.address,
        			tl_placebirth: body.placebirth,
        			tl_datebirth: body.datebirth,
        			tl_bio: body.bio,
        			tl_photo: body.photo,
        			tl_isactive: 0,
        		}
        		const insertData = await Database.talent.insertOne(talentRegister)
        		const verifAccount = {
        			vr_id: this.generateID(),
        			vr_talent_id: talentID,
        			vr_desc: body.desc
        		}
        		const insertVerif = await Database.verif_talent.insertOne(verifAccount)
        		/*Sending Email notif for verification*/

        		const OTP =  await this.getKodeOTP(talentRegister.tl_email, 'talent');
        		const Email = await this.sendingEmail({accountid: talentRegister.tl_id, email: talentRegister.tl_email, otp: OTP.kode})

        		if(insertData.state && insertVerif.state && Email){
        			delete body.password;
        			res.send({
        				state: true,
        				code: 100,
                        message: `Masukkan Kode OTP yang kami kirim ke Email ${body.email}`,
        				data: {
                            message: `Masukkan Kode OTP yang kami kirim ke Email ${body.email}`,
                            ...body
                        }
        			})
        		}else{
        			res.send({
        				state: false,
        				code: 102,
        				message: 'Failed to Register Talent Account, Error on inserting to Database',
        				error: {
        					insertData,
        					insertVerif,
        					Email
        				}
        			})
        		}
        	}else{
        		res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
        		})
        	}
        }catch(err){
        	console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'RegisterController');
        	res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
        }
	}

	ValidateRegistration = async (req, res) => {
		let fields = ['otp', 'email', 'type'];
		let newBody = Object.keys(req.body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)
        try{
	       const { otp, email, type } = req.body;
        	if(diff.length === 0){
        		// Validasi Account
        		let Account = type === 'user' ? (
        				await Database.profile_user.allSelect({pr_email: email, pr_isactive: 0})
        			) : (
        				await Database.talent.allSelect({tl_email: email, tl_isactive: 0})
        			);

        		if(Account.length > 0){
        			Account = Account[0];
        			// validate OTP
        			let OTP = await Database.otp_list.allSelect({otp_kode: otp, otp_status: 0});
        			if(OTP.length > 0){
        				OTP = OTP[0];
        				// Update Email Sending
        				// Update OTP LIST
        				// Update Profile isactive
        				let email, profileid, name
        				if(type === 'user'){
        					email = Account.pr_email
        					profileid = Account.pr_id
        					name = Account.pr_fullname
        				}else if(type === 'talent'){
        					email = Account.tl_email;
        					profileid = Account.tl_id
        					name = Account.tl_fullname
        				}

        				// const updateEmail = await Database.email_send.updateOne({email_to: email, email_status: 0}, {email_status: 1});
        				const updateOTP = await Database.otp_list.updateOne({otp_kode: otp, otp_status: 0}, {otp_status: 1});

        				if(updateOTP.state){
        					// Update and sending Email verification was success
        					let updateAccount = type === 'user' ? await Database.profile_user.updateOne({pr_id: profileid, pr_email: email}, {pr_isactive: 1}) : await Database.talent.updateOne({tl_id: profileid, tl_email: email}, {tl_isactive: 1})
        					if(updateAccount.state){
        						// Sending Email
        						const DateNow = this.createDate(0);
        						await this.SendingActiveEmail({NAME: name, DATE: DateNow, DATESTRING: this.createDateFormat(DateNow), EMAIL: email, TYPE: type})
        						return res.send({
			        				state: true,
			        				code: 100,
			        				message: `Success to Verify Account Type: ${type}, please login to use this Account`
			        			})
        					}else{
        						return res.send({
			        				state: false,
			        				code: 105,
			        				message: 'Failed to Verify, please contact Customer Service for Help'
			        			})
        					}
        				}else{
        					return res.send({
		        				state: false,
		        				code: 104,
		        				message: 'Failed to Verify, please Try Again'
		        			})
        				}
        			}else{
        				return res.send({
	        				state: false,
	        				code: 103,
	        				message: 'Failed to Verify OTP Code, please Try Again'
	        			})
        			}
        		}else{
        			return res.send({
        				state: false,
        				code: 102,
        				message: 'Failed to Get Account, account not Found'
        			})
        		}
        	}else{
        		return res.send({
        			state: false,
        			code: 101,
        			message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
                })
        	}
        }catch(err){
        	console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'RegisterController');
        	return res.send({
        		state: false,
        		code: 500,
        		message: 'Errors',
        		errors: err.message
        	})
        }
	}

    RequestOTP = async (req, res) => {
        res.send(true)
        let fields = ['email', 'type'];
        let newBody = Object.keys(req.body);
        let diff = fields.filter((x) => newBody.indexOf(x) === -1)
        try{
            const { body } = req;
            if(diff.length === 0){
                let Account = body.type === 'user' ? (
                    await Database.profile_user.allSelect({pr_email: body.email, pr_isactive: 0})
                ) : (
                    await Database.talent.allSelect({tl_email: body.email, tl_isactive: 0})
                );

                if(Account.length > 0){
                    let validateMax = await Database.otp_list.connection.raw(`select * from otp_list where otp_created_at BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW() AND otp_status = 0 AND otp_output = '${body.email}' AND otp_type = '${body.type}'`)
                    validateMax = validateMax.rows;
                    if(validateMax.length < 3){
                        let Email = type === 'user' ? Account.pr_email : Account.tl_email;
                        let ID = type === 'user' ? Account.pr_id : Account.tl_id;
                        const OTP =  await this.getKodeOTP(Email, 'user');
                        const EmailSend = await this.sendingEmail({accountid: ID, email: Email, otp: OTP.kode})
                        if(EmailSend){
                            return res.send({
                                state: true,
                                code: 100,
                                message: `Email terkirim, silahkan cek email untuk mendapatkan Kode OTP untuk Validasi Registrasi, Type : ${body.type}`,
                                data: {
                                    email: body.email,
                                    type: body.type
                                }
                            })
                        }else{
                            // Gagal Sending Email
                            return res.send({
                                state: false,
                                code: 103,
                                message: 'Gagal mengirimkan Email, silahkan coba beberapa saat lagi'
                            })
                        }
                    }else{
                        // Sudah Maksimal
                        return res.send({
                            state: false,
                            code: 102,
                            message: 'Akun anda sudah memiliki batas maksimal pengiriman Kode OTP Untuk registrasi perhari',
                        })
                    }

                }else{
                    /*Account sudah Aktif*/
                    return res.send({
                        state: false,
                        code: 100,
                        message: 'Akun sudah Aktif, silahkan masuk ke halaman Login',
                    })
                }
            }else{
                return res.send({
                    state: false,
                    code: 101,
                    message: `Input Not Valid, Missing Parameter : '${diff.toString()}'`
                })
            }
        }catch(err){
            console.log(err)
            await this.sendErrorDatabase(0, err.message, err.stack, 'RegisterController');
            return res.send({
                state: false,
                code: 500,
                message: 'Errors',
                errors: err.message
            })
        }
        // Request NEW OTP
        // Max 3 Kali
        // let query = `select * from otp_list where otp_created_at BETWEEN NOW() - INTERVAL '24 HOURS' AND NOW() AND otp_status = 0`;
        // let data = 
	}
	
}

export default new RegisterController