import MainController from './MainController';
import Database from '../Database/Database';
import Busboy from 'connect-busboy';
import Multer from 'multer';

import Path from 'path';
import fs from 'fs-extra';
import fsMorm from 'fs';

const basename = Path.basename(__filename);



class UploadController extends MainController{
	constructor () {
		super();
		this.uploadPath = Path.join(__dirname, '../../../Source/')
		try{
			fs.ensureDir(this.uploadPath);
		}catch(err){
			console.log(err);
            this.sendErrorDatabase(0, err.message, err.stack, 'UploadController');
		}
	}

	storage = Multer.diskStorage({
		destination: Path.join(__dirname + '../../../../Source/'),
		filename: (req, file, cb) => {
			let name= file.originalname.split('.')
	        name[0] = name[0].replace(/\s/g,'');
	        name[0] = name[0].replace(/[^0-9a-z]/gi, '');
	        name = `${this.generateID()}_${name[0]}.${name[name.length-1]}`;
			cb(null, name)
		}
	})

	upload = Multer({
		storage: this.storage,
		limits: {
			fileSize: 999999999
		}
	})

	UploadingFilesV1 = async (req, res) => {
        req.pipe(req.busboy)

        let imageCount = 0;
        var imageRespon = [];
        var dataSource = {};

        req.busboy.on('file', (fieldname, file, filename, encoding, mime) => {
            let name= filename.split('.')
            let typeFiles = mime.split('/')
            name[0] = name[0].replace(/\s/g,'');
            name[0] = name[0].replace(/[^0-9a-z]/gi, '');
            name = `${this.generateID()}_${name[0]}.${name[name.length-1]}`;

            const fstream = fs.createWriteStream(Path.join(this.uploadPath, name))

            imageCount++;

            file.pipe(fstream);

            dataSource[fieldname] = name;


            fstream.on('close', () => {
                let size = fs.statSync(Path.join(this.uploadPath, name));
                console.log(name, 'size', (size['size'] / 1048576))
                imageRespon.push({
                    name: name,
                    type: mime,
                    size: (size['size'] / 1048576)
                })
                file.unpipe(fstream);
            });

            fstream.on('error', (err) => {
                console.log('asdasdasd',err)
            })
        })

        req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
            dataSource[fieldname] = val;
        });

        req.busboy.on('finish', function(){
            req.unpipe(req.busboy);
            res.send({
            	code: 100,
            	state: true,
            	message: 'Berhasil Upload File',
            	data: {
	                fileLength: imageCount,
	                fieldData: dataSource
	                // image: imageRespon
            	}
            })
        })

        req.busboy.on('error', function(){
            console.log('eError on Busboy')
            res.send({
            	data: {},
            	code: 101,
            	state: false,
            	message: "Gagal Upload File"
            })
        })
	}

	UploadingFilesV2 = (req, res) => {
		const file = req.file;
		if(!file){
			res.send({
            	data: {},
            	code: 101,
            	state: false,
            	message: "Gagal Upload File"
            })
		}

		res.send({
			state: true,
			code: 100,
			message: 'Success Upload File',
			data: file
		});
	}
}

module.exports = new UploadController;