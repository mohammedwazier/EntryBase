import MainController from './MainController';
import Database from '../Database/Database';

import Path from 'path';
import FS from 'fs';
import FFMPEG from 'ffmpeg';

import MimeType from 'mime-types';

/*Library*/
// import Redis from 'redis';

// const client = Redis.createClient()
/*End Library*/

class VideoController extends MainController{
	constructor () {
		super();
	}

    startsWith = (str, prefix) => {
        return str.lastIndexOf(prefix, 0) === 0;
    }

    endsWith = (str, suffix) => {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    IndexVideo = async (req, res) => {
        const video = await Database.setting.single({st_kode: 'v_login'});
        const videoPath = Path.join(__dirname, '../../../Source/'+video.st_value)
        const mime = await MimeType.lookup(videoPath);
        res.set('Content-Type', mime);
        const fileStream = FS.createReadStream(videoPath);
        fileStream.pipe(res);
    }


}

export default new VideoController