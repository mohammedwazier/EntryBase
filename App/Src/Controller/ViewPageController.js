import MainController from './MainController';
import Database from '../Database/Database';

class ViewPageController extends MainController {
    constructor() {
        super();
    }

    VerificationRegister = async (req, res) => {
        const {
            verification
        } = req.params;
        console.log(verification)
        let template = await Database.setting.single({
            st_kode: 't_sregister'
        });

        let dataUser = await Database.profile_user.allSelect({pr_registrationhash: verification});
        let linkWebsite = await Database.setting.single({
            st_kode: 'l_link'
        })
        if(dataUser.length > 0){
            dataUser = dataUser[0];
            /* Checked isactive or not */
            if(parseInt(dataUser.pr_isactive) === 0){
                await Database.profile_user.updateOne({
                    pr_id: dataUser.pr_id,
                    pr_registrationhash: verification
                }, {
                    pr_updated: this.createDate(0),
                    pr_isactive: 1
                });
                return res.render(template.st_value, {
                    fullname: dataUser.pr_fullname,
                    created: this.createDate(0),
                    date: this.createDateFormat(this.createDate(0)),
                    link: linkWebsite.st_value
                });
            }else{
                return res.redirect(linkWebsite.st_value);
            }
        }else{
            return res.send({
                state: false,
                code: 109,
                message: 'Invalid Input Data',
                link: linkWebsite.st_value
            })
        }

    }
}

export default new ViewPageController