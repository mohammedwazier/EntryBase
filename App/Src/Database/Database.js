const db = {};
[
    'access_api',
    'admin',
    'comment',
    'cronjons_detail',
    'email_send',
    'error_logs',
    'following',
    'likes',
    'login',
    'media',
    'member_list',
    'otp_list',
    'product',
    'profile_user',
    'setting',
    'talent',
    // 'talent_page', /*Unused*/
    'verif_talent',
    'video',
    'view',
]
.forEach(file => {
    const modelName = file;
    let dataClass = require('../Helper/DatabaseHelper')
    dataClass = new dataClass(modelName)
    db[modelName] = dataClass
})

module.exports = db;