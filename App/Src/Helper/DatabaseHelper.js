const Connection = require('../Connection/Postgre');
// const SendErrorController = require('/SendErrorController');
const { STRUCTURE } = require('../Config/Config');

class DatabaseController {
    static table;
    static response;
    static connection;
    constructor (table) {
        this.table = table;
        this.response = STRUCTURE;
        this.connection = Connection;
        Connection.raw('select 1+1 as result')
        .catch((err) => {
            console.log('Connection Error to Database', err)
        })
    }

    ValidateData = (list) => {
        return new Promise(async (resolve) => {
            var response = this.response;
            try{
                if(Boolean(Array.isArray(list))){
                    let res = list;
                    response.data = res;
                }else{
                    let res = list;
                    response.data = res;
                }
                resolve(response.data);
            }catch(err){
                response.data = [];
                // SendErrorController.message(`Error on DatabaseController\n${JSON.stringify(err)}`)
                resolve(response.data)

            }
        })
    }

    ValidateResult = (result) => {
        return new Promise((resolve) => {
            try{
                if(result.command === 'INSERT'){
                    if(result.rowCount === 0){
                        throw Error;
                    }
                    resolve({state: true, message: `Success ${result.command} to ${this.table}`});
                }else if(result.command === 'UPDATE'){

                }
            }catch(err){
                // SendErrorController.message(`Error on DatabaseController\n${JSON.stringify(err)}`)
                resolve({
                    state: false,
                    message: `Failed to ${result.command} on Table ${this.table}`
                })
            }
        })
    }

    ValidateUpdate = (result) => {
        return new Promise(resolve => {
            try{
                if(Number(result) > 0){
                    return resolve({state: true, message: `Success`});
                }
                throw err
            }catch(err){
                // SendErrorController.message(`Error on DatabaseController\n${JSON.stringify(err)}`)
                return resolve({state: false, message: `Failed`});
            }
        })
    }

    select = (fields) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .select(fields)
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    selectField = (fields, where) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .select(fields)
            .where(where)
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    selectFieldOne = (fields, where) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .select(fields)
            .where(where)
            .first()
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    selectOne = (fields) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .select(fields)
            .first()
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    all = () => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    allSelect = (where) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .where(where)
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    single = (where) => {
        return new Promise(async(resolve) => {
            Connection(this.table)
            .where(where)
            .first()
            .then(this.ValidateData)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    insertOne = (insert) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .insert(insert)
            .then(this.ValidateResult)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    insert = (insert) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .insert(insert)
            .then(this.ValidateResult)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    updateOne = (where, update) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .where(where)
            .update(update)
            .then(this.ValidateUpdate)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }

    deleteOne = (where) => {
        return new Promise(async (resolve) => {
            Connection(this.table)
            .where(where)
            .del()
            .then(this.ValidateUpdate)
            .then(resolve)
            .catch((err) => resolve({state: false, message: err.stack}))
        })
    }
}
module.exports = DatabaseController;