// const D = require('@Model/index');
import jwt from 'jsonwebtoken';
import { req, res, next } from 'express'
import Database from '../Database/Database';
const { CIPHERID, ENV } = require('../Config/Config');

const verifyToken = async (req, res, next) => {
    try{
        let token = req.headers['authorization'] || req.headers.authorization;
        token = token.length > 0 ? token.split(' ')[1] : '';
        if(!token){
            return res.status(503).send({auth: false, state:false, code:500, message: 'No Token on this Request'});
        }

        jwt.verify(token, CIPHERID, async (err, decoded) => {
            if(err){
                return res.status(504).send({auth: false, state:false, code:110, message: 'Failed to authenticate token, try again'});
            }
            let validasi = await Database.login.allSelect({log_profile_id: decoded.id, log_token: token, log_status: 1});
            if(Number(validasi.length) === 0){
                return res.status(500).send({auth: false, state:false, code:500, message: 'Token Not Valid'});
            }
            next();
        })
    }catch(err){
        console.log(err)
        return res.status(502).send({auth: false, state:false, code:500, message: 'Failed to authenticate token'});
    }
}

module.exports = verifyToken;