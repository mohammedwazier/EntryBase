import Database from '../Database/Database';
import MainController from '../Controller/MainController'

// const Controller = MainController();


const ErrorHandler = async (err, req, res, next) => {
	// console.log('Error Middleware',err)
	switch(req && req.code){
		case 'ECONNRESET': {
			await MainController.sendErrorDatabase(err.code, err.message, err.stact, 'ErrorHandler')
			return res.status(504).send({
				state: false,
				code: 504,
				message: 'Connection Reset on Some Service, please try again soon'
			});
		} break;
		case 'ECONNREFUSED' : {
			await MainController.sendErrorDatabase(err.code, err.message, err.stact, 'ErrorHandler')
			return res.status(505).send({
				state: false,
				code: 504,
				message: 'Connection Refused to Some Service, please try again soon'
			});
		} break;
		default: {
			await MainController.sendErrorDatabase(err.code, err.message, err.stact, 'ErrorHandler')
			console.log('Error disini dapetnya', req)
			next(err)
		} break;
	}
}

module.exports = {
	ErrorHandler
}