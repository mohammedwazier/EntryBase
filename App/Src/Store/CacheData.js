/* 
Redis temporary Storing data in variable and in redis memory
Used for
1. Getting list
 */

import redis from 'redis';
import MainController from '../Controller/MainController';

class CacheData extends MainController{
    constructor(storeKey){
        super();
        const port = 7069 || 6379;
        const host = 'localhost';
        this.redisConn = redis.createClient(port, host);

        this.name = storeKey;
        this.value; /* String */

        this.redisConn.on('connect', function(data){
            console.log('Cache Store Data Connected',data);
            /* store to function Data process */
        })

        this.redisConn.on('error', function(err){
            console.log('Something Wrong', err.stack);
            this.sendErrorDatabase(err.code, err.message, err.stack, 'CacheData.js');
        })
        
    }

    save = (value) => {
        this.value = value;
        this.redisConn.set(this.name, JSON.stringify(value));
    }

    get = () => {
        return new Promise(async resolve => {
            this.redisConn.get(this.storeKey, (err, value) => {
                if(err){
                    return resolve({
                        state: false
                    })
                }
                return resolve({
                    state: true,
                    value: value
                })

            })
        })
    }
    
}

export default CacheData;