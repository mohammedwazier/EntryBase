const SocialMedia = [
	{
		name: 'Facebook',
		image: 'https://www.facebook.com/images/fb_icon_325x325.png'
	},
	{
		name: 'Twitter',
		image: 'https://abs.twimg.com/favicons/twitter.ico'
	},
	{
		name: 'WeChat',
		image: 'https://res.wx.qq.com/a/wx_fed/assets/res/NTI4MWU5.ico'
	},
	{
		name: 'Tumblr',
		image: 'https://assets.tumblr.com/images/msfavicon.png?_v=245323c5cb69e705ea213d9ed60e543a'
	},
	{
		name: 'Instagram',
		image: 'https://www.instagram.com/static/images/ico/favicon-192.png/68d99ba29cc8.png'
	},
	{
		name: 'Skype',
		image: 'https://secure.skypeassets.com/apollo/2.1.1591/images/icons/favicon.ico'
	},
	{
		name: 'Line',
		image: 'https://line.me/apple-touch-icon-precomposed.png'
	},
	{
		name: 'Snapchat',
		image: 'https://accounts.snapchat.com/accounts/static/images/favicon/favicon.png'
	},
	{
		name: 'Pinterest',
		image: 'https://s.pinimg.com/webapp/style/images/logo_trans_144x144-642179a1.png'
	},
	{
		name: 'LinkedIn',
		image: 'https://static-exp1.licdn.com/scds/common/u/images/logos/favicons/v1/favicon.ico'
	},
	{
		name: 'Telegram',
		image: 'https://web.telegram.org/favicon.ico'
	},
	{
		name: 'WhatsApp',
		image: 'https://static.whatsapp.net/rsrc.php/v3/yP/r/rYZqPCBaG70.png'
	},
	// {
	// 	name: 'Reddit'
	// },
	{
		name: 'Spotify',
		image: 'https://www.scdn.co/i/_global/favicon.png'
	},
	{
		name: 'Youtube',
		image: 'https://s.ytimg.com/yts/img/favicon_144-vfliLAfaB.png'
	},
	{
		name: 'Vine',
		image: 'https://v.cdn.vine.co/w/a001567d-assets/images/favicon.ico'
	},
	{
		name: 'Meetup',
		image: 'https://www.meetup.com/gb_static/icons/icon-48x48.png?v=91182cf513c87a58169c7562933752e4'
	},
	{
		name: 'Discord',
		image: 'https://discord.com/assets/07dca80a102d4149e9736d4b162cff6f.ico'
	},
	{
		name: 'TikTok',
		image: 'https://s16.tiktokcdn.com/musical/resource/wap/static/image/logo_144c91a.png?v=2'
	},
	{
		name: 'Medium',
		image: 'https://cdn-static-1.medium.com/_/fp/icons/favicon-rebrand-medium.3Y6xpZ-0FSdWDnPM3hSBIA.ico'
	},
	{
		name: 'Quora',
		image: 'https://qsf.fs.quoracdn.net/-3-images.favicon.ico-26-ae77b637b1e7ed2c.ico'
	}
]

module.exports = {
	SocialMedia
}