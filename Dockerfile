#NODE PACKAGES INSTALL
FROM node:13-slim

ADD . /entrymicroservice
WORKDIR /entrymicroservice

RUN yarn global add pm2
RUN yarn install
RUN yarn run build:test

EXPOSE 3000

CMD ["pm2-docker", "start", "dev_ecosystem.config.js"]