# EntryBase
EntryBase Service

![alt text](https://entry.show/images/logo_only.png?raw=true)

### Mapping Microservice PORT
| Name			| PORT		| Endpoint 				| Desc 	| Progress |
| ------------- |:----:     | -----						| ---	| ----	|
| Index 		| **3000** | /		 					|		| 0% |
| Login			| **3001** | /api/v1/Login				|		| 50% |
| Register		| **3002** | /api/v1/Register			|		| 0% |
| Upload		| **3003** | /api/v1/Upload				|		| 0% |
| Profile		| **3004** | /api/v1/Profile			|		| 0% |
| Video			| **3005** | /api/v1/Video				|		| 0% |
| Produk		| **3006** | /api/v1/Produk				|		| 0% |
| Endorse		| **3007** | /api/v1/Endorse			|		| 0% |
| Utilization 	| **3008** | /api/v1/Utilization		| 		| 0% |
| Following 	| **3009** | /api/v1/Following  		| 		| 0% |
| View HTML Page | **3010** | /view         	    	| 		| 0% |

---
### Running
**PM2 and Nodemon**
```
git clone https://github.com/mohammedwazier/EntryBase.git
npm/yarn install
yarn global add nodemon pm2 / npm install -g nodemon pm2
sh run.sh (Development)
```

**Docker**
```
yarn docker:build
yarn docker:run
```

**run with Docker Compose**
```
clone the docker-compose.yml file
then docker-compose up
```

### Build Status
![Node.js CI](https://github.com/mohammedwazier/EntryBase/workflows/Node.js%20CI/badge.svg?branch=master)
