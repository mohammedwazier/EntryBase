module.exports = {
  apps : [
    {
      name: '[DEV][Entry] Main Server',
      script: 'Build/Service/Index.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Login Service',
      script: 'Build/Service/Login.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Register Service',
      script: 'Build/Service/Register.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Upload Service',
      script: 'Build/Service/Upload.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Profile Service',
      script: 'Build/Service/Profile.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Video Service',
      script: 'Build/Service/Video.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Produk Service',
      script: 'Build/Service/Produk.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Endorse Service',
      script: 'Build/Service/Endorse.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Utilization Service',
      script: 'Build/Service/Utilization.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    },
    {
      name: '[DEV][Entry] Following Service',
      script: 'Build/Service/Following.js',
      args: '',
      next_gen_js: true,
      instances: 1,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: false,
      watch:true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'development',
        TZ: "Asia/Jakarta"
      }
    }
  ],

  deploy : {
    development: {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload dev_ecosystem.config.js --env development',
      'pre-setup': ''
    },
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload dev_ecosystem.config.js --env development',
      'pre-setup': ''
    }
  }
};
