module.exports = {
  apps : [
    {
      name: '[PROD] Main Server',
      script: 'Build/Index.js',
      args: '',
      next_gen_js: true,
      instances: 4,
      exec_mode: 'cluster',
      merge_logs: true,
      log_date_format: 'YYYMMDD',
      autorestart: true,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      },
      env_production: {
        NODE_ENV: 'production',
        TZ: "Asia/Jakarta"
      }
    }
  ],

  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production',
      'pre-setup': ''
    }
  }
};
