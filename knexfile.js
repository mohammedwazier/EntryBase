require('dotenv').config();

const { ENV } = process.env;

if(ENV === 'local'){
    var knexFile = require('./App/Src/Connection/KnexFile');
}else{
    var knexFile = require('./Build/Src/Connection/KnexFile');
}

module.exports = knexFile