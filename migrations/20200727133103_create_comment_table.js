
exports.up = function(knex) {
	return knex.schema.createTable('comment', function(table){
		// table.bigIncrements();
		table.increments('id').primary();

		table.string('cm_id').notNull();
		table.string('cm_profileid').notNull();
		// table.string('cm_text').notNull();
		table.specificType('cm_text', 'character varying').nullable();
		table.datetime('cm_created').defaultTo(knex.fn.now());
		table.datetime('cm_deketed').nullable();
		table.integer('cm_isactive').defaultTo(1);
		table.integer('cm_isdelete').defaultTo(0);
		table.integer('cm_countreport').defaultTo(0);
		table.string('cm_videoid').notNull();

		table.index(['cm_id', 'cm_profileid', 'cm_videoid'], 'comment_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('comment');
};
