
exports.up = function(knex) {
	return knex.schema.createTable('cronjobs_detail', function(table){
		table.increments('id').primary();

		table.string('job_id').nullable();
		table.string('job_code').nullable();
		table.datetime('job_starttime').defaultTo(knex.fn.now());
		table.datetime('job_endtime').nullable();
		table.string('job_status').defaultTo('false');
		// table.string('job_message').nullable();
		table.specificType('job_message', 'character varying').nullable();
		table.datetime('job_created').defaultTo(knex.fn.now());

		table.index(['job_id', 'job_code', 'job_status'], 'cronhobs_index');
	});
};

exports.down = function(knex) {
	return knex.schema.dropTable('cronjobs_detail');
};
