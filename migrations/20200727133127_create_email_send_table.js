
exports.up = function(knex) {
	return knex.schema.createTable('email_send', function(table){
		table.increments('id').primary();

		table.string('email_id').nullable();
		table.string('email_from').nullable();
		table.string('email_host').nullable();
		table.string('email_status', 2).defaultTo(1);
		table.string('email_to').nullable();
		// table.string('email_html').nullable();
		table.specificType('email_html', 'character varying').nullable();
		// table.string('email_text').nullable();
		table.specificType('email_text', 'character varying').nullable();
		table.datetime('email_created_at').defaultTo(knex.fn.now());
		table.datetime('email_updated_at').nullable();
		// table.string('email_jsonraw').nullable();
		table.specificType('email_jsonraw', 'character varying').nullable();
		table.string('email_messageid').nullable();

		table.index(['email_id', 'email_from', 'email_to'], 'email_send_index');
	});
};

exports.down = function(knex) {
	return knex.schema.dropTable('email_send');
};
