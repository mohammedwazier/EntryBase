
exports.up = function(knex) {
	return knex.schema.createTable('error_logs', function(table){
		table.increments('id').primary();

		table.string('err_code').nullable();
		table.string('err_message').nullable();
		// table.string('err_data').nullable();
		table.specificType('err_data', 'character varying').nullable();
		table.string('err_func').nullable();
		table.datetime('err_created').defaultTo(knex.fn.now());

		table.index(['err_code', 'err_func'], 'error_logs_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('error_logs');
};
