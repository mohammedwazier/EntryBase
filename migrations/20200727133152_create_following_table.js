
exports.up = function(knex) {
	return knex.schema.createTable('following', function(table){
		table.increments('id').primary();

		table.string('fl_id').unique();
		table.string('fl_talentid').nullable();
		table.string('fl_userid').nullable();
		table.datetime('fl_created_at').defaultTo(knex.fn.now());
		table.integer('fl_status').defaultTo(1);

		table.index(['fl_id', 'fl_talentid', 'fl_userid'], 'following_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('following');
};
