
exports.up = function(knex) {
	return knex.schema.createTable('likes', function(table){
		table.increments('id').primary();

		table.string('l_id').unique();
		table.string('l_prodid').nullable();
		table.string('l_userid').nullable();
		table.datetime('l_created_at').defaultTo(knex.fn.now());

		table.index(['l_prodid', 'l_userid'], 'likes_index');
	});
};

exports.down = function(knex) {
	return knex.schema.dropTable('likes')
};
