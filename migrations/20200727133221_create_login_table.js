
exports.up = function(knex) {
	return knex.schema.createTable('login', function(table){
		table.increments('id').primary();

		table.string('log_profile_id').nullable();
		// table.string('log_token').nullable();
		table.specificType('log_token', 'character varying').nullable();
		table.string('log_type').nullable();
		table.datetime('log_created_date').defaultTo(knex.fn.now());
		table.integer('log_status').defaultTo(1);
		table.string('log_ip').nullable();
		// table.string('log_data').nullable();
		table.specificType('log_data', 'character varying').nullable();
		table.string('log_lat').nullable();
		table.string('log_ing').nullable();
		table.string('log_firebase_token').nullable();

		table.index(['log_profile_id', 'log_ip'], 'login_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('login');
};
