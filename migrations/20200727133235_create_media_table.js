
exports.up = function(knex) {
	return knex.schema.createTable('media', function(table){
		table.increments('id').primary();

		table.string('md_id').unique();
		// table.string('md_filename').unique();
		table.specificType('md_filename', 'character varying').nullable();
		table.string('md_mime').nullable();
		table.string('md_size').nullable();
		// table.string('md_realname').nullable();
		table.specificType('md_realname', 'character varying').nullable();
		// table.string('md_data').nullable();
		table.specificType('md_data', 'character varying').nullable();
		table.date('md_created_at').defaultTo(knex.fn.now());
		table.string('md_profileupload').nullable();
		table.string('md_typeupload').nullable();
		table.integer('md_deleted').defaultTo(0);

		table.index(['md_id', 'md_profileupload'], 'media_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('media');
};
