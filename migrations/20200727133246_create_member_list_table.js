
exports.up = function(knex) {
	return knex.schema.createTable('member_list', function(table){
		table.increments('id').primary();

		table.string('ml_id').unique();
		table.string('ml_profileid').nullable();
		table.string('ml_photo').nullable();
		table.string('ml_photothumb').nullable();
		table.date('ml_created').defaultTo(knex.fn.now());
		table.datetime('ml_deleted').nullable();
		table.datetime('ml_updated').nullable();
		table.integer('ml_isdelete').defaultTo(0);
		table.integer('ml_isactive').defaultTo(1);
		table.string('ml_talentid').nullable();

		table.index(['ml_id', 'ml_profileid', 'ml_talentid'], 'memberlist_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('member_list')
};
