
exports.up = function(knex) {
	return knex.schema.createTable('otp_list', function(table){
		table.increments('id').primary();

		table.string('otp_id').unique();
		table.string('otp_output').nullable();
		table.string('otp_kode').nullable();
		table.integer('otp_status').defaultTo(0);
		table.datetime('otp_created_at').defaultTo(knex.fn.now());
		table.datetime('otp_updated_at').nullable();
		table.string('otp_type').nullable();
		table.datetime('otp_expired').nullable();

		table.index(['otp_id', 'otp_output', 'otp_kode'], 'otp_list_index');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('otp_list');
};
