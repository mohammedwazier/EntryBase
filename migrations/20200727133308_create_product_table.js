
exports.up = function(knex) {
	return knex.schema.createTable('product', function(table){
		table.increments('id').primary();

		table.string('prod_id').unique();
		table.string('prod_code').nullable();
		table.string('prod_name').nullable();
		table.string('prod_isbuy').nullable();
		table.bigInteger('prod_cost').defaultTo(0);
		table.string('prod_title').nullable();
		table.string('prod_desc').nullable();
		table.integer('prod_buy').defaultTo(0);
		table.string('prod_type').nullable();
		table.datetime('prod_created').defaultTo(knex.fn.now());
		table.datetime('prod_updated').nullable();
		// table.string('prod_data', 10000).nullable();
		table.specificType('prod_data', 'character varying'); /* Only postgres */
		table.string('prod_video').nullable();
		table.string('prod_videothumb').nullable();
		table.string('prod_imgthumb').nullable();
		table.integer('prod_createvideothumb').defaultTo(0);
		table.string('prod_talentid').nullable();

		table.index(['prod_id', 'prod_code', 'prod_name'], 'product_index');
		table.index(['prod_cost', 'prod_isbuy'], 'product_buy');
		table.index(['prod_title'], 'product_title');
		table.index(['prod_talentid'], 'product_talent');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('product');
};
