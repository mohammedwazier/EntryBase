
exports.up = function(knex) {
	return knex.schema.createTable('profile_user', function(table){
		table.increments('id').primary();
		table.string('pr_id').unique();
		table.string('pr_username').unique();
		table.string('pr_password').notNull();
		table.string('pr_email').unique();
		table.string('pr_countrycode').nullable();
		table.string('pr_numberphone').unique();
		table.string('pr_combinephone').unique();
		table.string('pr_fullname').nullable();
		table.string('pr_address').nullable();
		table.string('pr_province').nullable();
		table.string('pr_city').nullable();
		table.string('pr_district').nullable();
		table.string('pr_subdistrict').nullable();
		table.string('pr_village').nullable();
		table.string('pr_zipcode').nullable();
		table.string('pr_placebirth').nullable();
		table.datetime('pr_datebirth').nullable();
		table.string('pr_gender').nullable();
		table.bigInteger('pr_saldo').defaultTo(0);
		table.bigInteger('pr_saldobefore').defaultTo(0);
		table.string('pr_role').defaultTo('USER');
		table.string('pr_statusrole').defaultTo('USER');
		table.integer('pr_isactive').defaultTo(1);
		table.integer('pr_isdelete').defaultTo(0);
		table.integer('pr_issuspend').defaultTo(0);
		table.datetime('pr_created').defaultTo(knex.fn.now());
		table.datetime('pr_updated').nullable();
		table.datetime('pr_deleted').nullable();
		table.string('pr_photo').nullable();
		table.string('pr_photothumb').nullable();
		table.integer('pr_upgrade').defaultTo(0);
		table.string('pr_idupgrade').nullable();
		table.integer('pr_upgradetalent').defaultTo(0)
		table.string('pr_idupgradetalent').nullable();
		table.string('pr_ktp').nullable();
		table.specificType('pr_registrationhash', 'character varying').nullable();
		table.specificType('pr_header', 'character varying').nullable();

		table.index(['pr_combinephone'], 'profile_user_pr_combinephone_key');
		table.index(['pr_email'], 'profile_user_pr_email_key');
		table.index(['pr_numberphone'], 'profile_user_pr_numberphone_key');
		table.index(['pr_username'], 'profile_user_pr_username_key');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('profile_user')
};
