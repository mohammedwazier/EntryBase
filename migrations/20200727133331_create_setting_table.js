
exports.up = function(knex) {
	return knex.schema.createTable('setting', function(table){
		table.increments('st_id').primary();
		table.string('st_keterangan').nullable()
		table.string('st_kode').unique();
		// table.string('st_value').nullable();
		table.specificType('st_value', 'character varying').nullable();
		table.datetime('st_created_at').defaultTo(knex.fn.now()).nullable();
		table.datetime('st_update_at').nullable();
		table.string('st_id_admin').nullable();
		table.string('st_isactive', 1).defaultTo(1);
		table.string('st_typevalue').nullable();

		table.index(['st_kode'], 'setting_st_kode_key');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('setting');
};
