
exports.up = function(knex) {
	/* Unused Table */
	/* 
	Rolenya menjadi 1 tabel di profile user
	 */
	/* return knex.schema.createTable('talent', function(table){
		table.increments('id').primary();

		table.string('tl_id').unique();
		table.string('tl_username').unique();
		table.string('tl_password').nullable();
		table.string('tl_email').nullable();
		table.string('tl_countrycode').nullable()
		table.string('tl_numberphone').unique();
		table.string('tl_combinephone').unique();
		table.string('tl_fullname').nullable();
		table.string('tl_address').nullable();
		table.string('tl_province').nullable();
		table.string('tl_city').nullable();
		table.string('tl_district').nullable();
		table.string('tl_subdistrict').nullable();
		table.string('tl_village').nullable();
		table.string('tl_zipcode').nullable();
		table.string('tl_placebirth').nullable();
		table.datetime('tl_datebirth').nullable();
		table.bigInteger('tl_saldo').defaultTo(0);
		table.bigInteger('tl_saldobefore').defaultTo(0);
		table.string('tl_bio').nullable();
		table.integer('tl_isactive').defaultTo(1);
		table.integer('tl_isdelete').defaultTo(0);
		table.integer('tl_issuspend').defaultTo(0);
		table.integer('tl_verified').defaultTo(0);
		table.datetime('tl_created').defaultTo(knex.fn.now());
		table.datetime('tl_updated').nullable();
		table.datetime('tl_deleted').nullable();
		table.string('tl_photo').nullable();
		table.string('tl_photo_thumb').nullable();
		table.string('tl_card').nullable();
		table.string('tl_header').nullable();

		table.index(['tl_combinephone'], 'talent_tl_combinephone_key');
		table.index(['tl_email'], 'talent_tl_email_key');
		table.index(['tl_numberphone'], 'talent_tl_numberphone_key');
		table.index(['tl_username'], 'talent_tl_username_key');
	}) */
};

exports.down = function(knex) {
	return knex.schema.dropTable('talent');
};
