
exports.up = function(knex) {
	return knex.schema.createTable('verif_talent', function(table){
		table.increments('id').primary();

		table.string('vr_id').unique();
		table.string('vr_talent_id').unique();
		table.integer('vr_status').defaultTo(0);
		table.datetime('vr_created_at').defaultTo(knex.fn.now());
		table.datetime('vr_updated_at').nullable();
		table.text('vr_desc').nullable();
		table.string('vr_kyc').nullable();

		table.index(['vr_talent_id'], 'verif_talent_vr_talent_id_key');
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('verif_talent');
};
