
exports.up = function(knex) {
	return knex.schema.createTable('video', function(table){
		table.increments('id').primary();

		table.string('vid_id').unique();
		table.string('vid_name').notNull();
		table.string('vid_desc').nullable();
		table.string('vid_genre');
		table.string('vid_talentid');
		table.integer('vid_view').defaultTo(0);
		table.integer('vid_likes').defaultTo(0);
		table.datetime('vid_created').defaultTo(knex.fn.now());
		table.datetime('vid_updated').nullable();
		table.datetime('vid_deleted').nullable();
		table.datetime('vid_archived').nullable();
		table.integer('vid_isactive').defaultTo(1);
		table.integer('vid_isarchived').defaultTo(0);
		table.integer('vid_isdeleted').defaultTo(0);
		table.string('vid_length').nullable();
		table.string('vid_thumb').nullable();
		table.string('vid_prodcode').nullable();
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('video');
};
