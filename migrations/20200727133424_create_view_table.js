
exports.up = function(knex) {
	return knex.schema.createTable('view', function(table){
		table.increments('id').primary();

		table.string('v_id').unique();
		table.string('v_prodid').nullable();
		table.string('v_userid').nullable();
		table.datetime('v_created_at').defaultTo(knex.fn.now());
	})
};

exports.down = function(knex) {
	return knex.schema.dropTable('view');
};
