
exports.up = function(knex) {
    return knex.schema.createTable('upgrade_business', function(table){
        table.increments('id').primary();

        table.string('upg_id').notNull().unique();
        table.string('upg_userid').notNull().unique();
        table.string('upg_nomerktp').notNull();
        table.string('upg_filektp').notNull();
        table.string('upg_fileselfie').notNull();
        table.integer('upg_status').defaultTo(0); /* 0 Not Verified, 1 Verified, 2 Declined */
        table.datetime('upg_created').defaultTo(knex.fn.now());
        table.datetime('upg_updated').nullable();
        table.string('upg_adminacc').nullable(); /* Admin ID Accept */
        table.datetime('upg_approved').nullable();

        table.index(['upg_userid', 'upg_nomerktp'], 'upgrade_bisnis_index');
        table.index(['upg_adminacc'], 'upgrade_bisnis_admin');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('upgrade_business');
};
