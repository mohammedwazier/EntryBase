
exports.up = function(knex) {
    return knex.schema.createTable('data_bank', function(table){
        table.increments('id').primary();

        table.string('bank_name').nullable();
        table.string('bank_code').nullable();
        table.string('bank_codename').nullable()
        table.string('bank_image').nullable();
        table.datetime('bank_created').defaultTo(knex.fn.now());
        table.datetime('bank_update').nullable();
        table.integer('bank_status').defaultTo(1);

        table.index(['bank_name', 'bank_code'], 'bank_index');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('data_bank');
};
