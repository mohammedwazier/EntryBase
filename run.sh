#!/bin/bash
concurrently --kill-others "nodemon App/Service/Index.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Login.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Register.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Upload.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Profile.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Video.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Produk.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Endorse.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Utilization.js --delay 500ms --exec babel-node" \
"nodemon App/Service/Following.js --delay 500ms --exec babel-node" \
"nodemon App/Service/ViewPage.js --delay 500ms --exec babel-node"