
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('data_bank').del()
    .then(function () {
      // Inserts seed entries
      return knex('data_bank').insert([
        {
          bank_name: "BANK BRI",
          bank_code: "002",
          bank_codename: "bri"
        }, {
          bank_name: "BANK EKSPOR INDONESIA",
          bank_code: "003"
        }, {
          bank_name: "BANK MANDIRI",
          bank_code: "008",
          bank_codename: "mandiri"
        }, {
          bank_name: "BANK BNI",
          bank_code: "009",
          bank_codename: "bni"
        }, {
          bank_name: "BANK DANAMON",
          bank_code: "011",
          bank_codename: "danamon"
        }, {
          bank_name: "PERMATA BANK",
          bank_code: "013",
          bank_codename: "permata"
        }, {
          bank_name: "BANK BCA",
          bank_code: "014",
          bank_codename: "bca"
        }, {
          bank_name: "BANK BII",
          bank_code: "016"
        }, {
          bank_name: "BANK PANIN",
          bank_code: "019"
        }, {
          bank_name: "BANK ARTA NIAGA KENCANA",
          bank_code: "020"
        }, {
          bank_name: "BANK NIAGA",
          bank_code: "022"
        }, {
          bank_name: "BANK BUANA IND",
          bank_code: "023"
        }, {
          bank_name: "BANK LIPPO",
          bank_code: "026"
        }, {
          bank_name: "BANK NISP",
          bank_code: "028"
        }, {
          bank_name: "AMERICAN EXPRESS BANK LTD",
          bank_code: "030"
        }, {
          bank_name: "CITIBANK N.A.",
          bank_code: "031",
          bank_codename: "citibank"
        }, {
          bank_name: "JP. MORGAN CHASE BANK, N.A.",
          bank_code: "032"
        }, {
          bank_name: "BANK OF AMERICA, N.A",
          bank_code: "033"
        }, {
          bank_name: "ING INDONESIA BANK",
          bank_code: "034"
        }, {
          bank_name: "BANK MULTICOR TBK.",
          bank_code: "036"
        }, {
          bank_name: "BANK ARTHA GRAHA",
          bank_code: "037",
          bank_codename: "artha"
        }, {
          bank_name: "BANK CREDIT AGRICOLE INDOSUEZ",
          bank_code: "039"
        }, {
          bank_name: "THE BANGKOK BANK COMP. LTD",
          bank_code: "040"
        }, {
          bank_name: "THE HONGKONG & SHANGHAI B.C.",
          bank_code: "041"
        }, {
          bank_name: "THE BANK OF TOKYO MITSUBISHI UFJ LTD",
          bank_code: "042",
          bank_codename: "tokyo"
        }, {
          bank_name: "BANK SUMITOMO MITSUI INDONESIA",
          bank_code: "045"
        }, {
          bank_name: "BANK DBS INDONESIA",
          bank_code: "046",
          bank_codename: "dbs"
        }, {
          bank_name: "BANK RESONA PERDANIA",
          bank_code: "047"
        }, {
          bank_name: "BANK MIZUHO INDONESIA",
          bank_code: "048"
        }, {
          bank_name: "STANDARD CHARTERED BANK",
          bank_code: "050"
        }, {
          bank_name: "BANK ABN AMRO",
          bank_code: "052"
        }, {
          bank_name: "BANK KEPPEL TATLEE BUANA",
          bank_code: "053"
        }, {
          bank_name: "BANK CAPITAL INDONESIA, TBK.",
          bank_code: "054"
        }, {
          bank_name: "BANK BNP PARIBAS INDONESIA",
          bank_code: "057"
        }, {
          bank_name: "BANK UOB INDONESIA",
          bank_code: "058",
          bank_codename: "uob"
        }, {
          bank_name: "KOREA EXCHANGE BANK DANAMON",
          bank_code: "059"
        }, {
          bank_name: "RABOBANK INTERNASIONAL INDONESIA",
          bank_code: "060"
        }, {
          bank_name: "ANZ PANIN BANK",
          bank_code: "061",
          bank_codename: "panin"
        }, {
          bank_name: "DEUTSCHE BANK AG.",
          bank_code: "067"
        }, {
          bank_name: "BANK WOORI INDONESIA",
          bank_code: "068"
        }, {
          bank_name: "BANK OF CHINA LIMITED",
          bank_code: "069",
          bank_codename: "boc"
        }, {
          bank_name: "BANK BUMI ARTA",
          bank_code: "076",
          bank_codename: "bumi_artha"
        }, {
          bank_name: "BANK EKONOMI",
          bank_code: "087"
        }, {
          bank_name: "BANK ANTARDAERAH",
          bank_code: "088"
        }, {
          bank_name: "BANK HAGA",
          bank_code: "089"
        }, {
          bank_name: "BANK IFI",
          bank_code: "093"
        }, {
          bank_name: "BANK CENTURY, TBK.",
          bank_code: "095"
        }, {
          bank_name: "BANK MAYAPADA",
          bank_code: "097",
          bank_codename: "mayapada"
        }, {
          bank_name: "BANK JABAR",
          bank_code: "110"
        }, {
          bank_name: "BANK DKI",
          bank_code: "111",
          bank_codename: "dki"
        }, {
          bank_name: "BPD DIY",
          bank_code: "112",
          bank_codename: "daerah_istimewa"
        }, {
          bank_name: "BANK JATENG",
          bank_code: "113",
          bank_codename: "jawa_tengah"
        }, {
          bank_name: "BANK JATIM",
          bank_code: "114",
          bank_codename: "jawa_timur"
        }, {
          bank_name: "BPD JAMBI",
          bank_code: "115",
          bank_codename: "jambi"
        }, {
          bank_name: "BPD ACEH",
          bank_code: "116"
        }, {
          bank_name: "BANK SUMUT",
          bank_code: "117",
          bank_codename: "sumut"
        }, {
          bank_name: "BANK NAGARI",
          bank_code: "118"
        }, {
          bank_name: "BANK RIAU",
          bank_code: "119"
        }, {
          bank_name: "BANK SUMSEL",
          bank_code: "120"
        }, {
          bank_name: "BANK LAMPUNG",
          bank_code: "121"
        }, {
          bank_name: "BPD KALSEL",
          bank_code: "122"
        }, {
          bank_name: "BPD KALIMANTAN BARAT",
          bank_code: "123"
        }, {
          bank_name: "BPD KALTIM",
          bank_code: "124"
        }, {
          bank_name: "BPD KALTENG",
          bank_code: "125"
        }, {
          bank_name: "BPD SULSEL",
          bank_code: "126"
        }, {
          bank_name: "BANK SULUT",
          bank_code: "127"
        }, {
          bank_name: "BPD NTB",
          bank_code: "128"
        }, {
          bank_name: "BPD BALI",
          bank_code: "129"
        }, {
          bank_name: "BANK NTT",
          bank_code: "130"
        }, {
          bank_name: "BANK MALUKU",
          bank_code: "131"
        }, {
          bank_name: "BPD PAPUA",
          bank_code: "132"
        }, {
          bank_name: "BANK BENGKULU",
          bank_code: "133"
        }, {
          bank_name: "BPD SULAWESI TENGAH",
          bank_code: "134"
        }, {
          bank_name: "BANK SULTRA",
          bank_code: "135"
        }, {
          bank_name: "BANK NUSANTARA PARAHYANGAN",
          bank_code: "145"
        }, {
          bank_name: "BANK SWADESI",
          bank_code: "146"
        }, {
          bank_name: "BANK MUAMALAT",
          bank_code: "147",
          bank_codename: "muamalat"
        }, {
          bank_name: "BANK MESTIKA",
          bank_code: "151"
        }, {
          bank_name: "BANK METRO EXPRESS",
          bank_code: "152"
        }, {
          bank_name: "BANK SHINTA INDONESIA",
          bank_code: "153"
        }, {
          bank_name: "BANK MASPION",
          bank_code: "157"
        }, {
          bank_name: "BANK HAGAKITA",
          bank_code: "159"
        }, {
          bank_name: "BANK GANESHA",
          bank_code: "161"
        }, {
          bank_name: "BANK WINDU KENTJANA",
          bank_code: "162"
        }, {
          bank_name: "HALIM INDONESIA BANK",
          bank_code: "164"
        }, {
          bank_name: "BANK HARMONI INTERNATIONAL",
          bank_code: "166"
        }, {
          bank_name: "BANK KESAWAN",
          bank_code: "167"
        }, {
          bank_name: "BANK TABUNGAN NEGARA (PERSERO)",
          bank_code: "200"
        }, {
          bank_name: "BANK HIMPUNAN SAUDARA 1906, TBK .",
          bank_code: "212"
        }, {
          bank_name: "BANK TABUNGAN PENSIUNAN NASIONAL",
          bank_code: "213"
        }, {
          bank_name: "BANK SWAGUNA",
          bank_code: "405"
        }, {
          bank_name: "BANK JASA ARTA",
          bank_code: "422"
        }, {
          bank_name: "BANK MEGA",
          bank_code: "426"
        }, {
          bank_name: "BANK JASA JAKARTA",
          bank_code: "427"
        }, {
          bank_name: "BANK BUKOPIN",
          bank_code: "441"
        }, {
          bank_name: "BANK SYARIAH MANDIRI",
          bank_code: "451",
          bank_codename: "bsm"
        }, {
          bank_name: "BANK BISNIS INTERNASIONAL",
          bank_code: "459"
        }, {
          bank_name: "BANK SRI PARTHA",
          bank_code: "466"
        }, {
          bank_name: "BANK JASA JAKARTA",
          bank_code: "472"
        }, {
          bank_name: "BANK BINTANG MANUNGGAL",
          bank_code: "484"
        }, {
          bank_name: "BANK BUMIPUTERA",
          bank_code: "485"
        }, {
          bank_name: "BANK YUDHA BHAKTI",
          bank_code: "490"
        }, {
          bank_name: "BANK MITRANIAGA",
          bank_code: "491"
        }, {
          bank_name: "BANK AGRO NIAGA",
          bank_code: "494"
        }, {
          bank_name: "BANK INDOMONEX",
          bank_code: "498"
        }, {
          bank_name: "BANK ROYAL INDONESIA",
          bank_code: "501"
        }, {
          bank_name: "BANK ALFINDO",
          bank_code: "503"
        }, {
          bank_name: "BANK SYARIAH MEGA",
          bank_code: "506"
        }, {
          bank_name: "BANK INA PERDANA",
          bank_code: "513"
        }, {
          bank_name: "BANK HARFA",
          bank_code: "517"
        }, {
          bank_name: "PRIMA MASTER BANK",
          bank_code: "520"
        }, {
          bank_name: "BANK PERSYARIKATAN INDONESIA",
          bank_code: "521"
        }, {
          bank_name: "BANK AKITA",
          bank_code: "525"
        }, {
          bank_name: "LIMAN INTERNATIONAL BANK",
          bank_code: "526"
        }, {
          bank_name: "ANGLOMAS INTERNASIONAL BANK",
          bank_code: "531"
        }, {
          bank_name: "BANK DIPO INTERNATIONAL",
          bank_code: "523"
        }, {
          bank_name: "BANK KESEJAHTERAAN EKONOMI",
          bank_code: "535"
        }, {
          bank_name: "BANK UIB",
          bank_code: "536"
        }, {
          bank_name: "BANK ARTOS IND",
          bank_code: "542"
        }, {
          bank_name: "BANK PURBA DANARTA",
          bank_code: "547"
        }, {
          bank_name: "BANK MULTI ARTA SENTOSA",
          bank_code: "548"
        }, {
          bank_name: "BANK MAYORA",
          bank_code: "553"
        }, {
          bank_name: "BANK INDEX SELINDO",
          bank_code: "555"
        }, {
          bank_name: "BANK VICTORIA INTERNATIONAL",
          bank_code: "566"
        }, {
          bank_name: "BANK EKSEKUTIF",
          bank_code: "558"
        }, {
          bank_name: "CENTRATAMA NASIONAL BANK",
          bank_code: "559"
        }, {
          bank_name: "BANK FAMA INTERNASIONAL",
          bank_code: "562"
        }, {
          bank_name: "BANK SINAR HARAPAN BALI",
          bank_code: "564"
        }, {
          bank_name: "BANK HARDA",
          bank_code: "567"
        }, {
          bank_name: "BANK FINCONESIA",
          bank_code: "945"
        }, {
          bank_name: "BANK MERINCORP",
          bank_code: "946"
        }, {
          bank_name: "BANK MAYBANK INDOCORP",
          bank_code: "947",
          bank_codename: "bii"
        }, {
          bank_name: "BANK OCBC – INDONESIA",
          bank_code: "948",
          bank_codename: "ocbc"
        }, {
          bank_name: "BANK CHINA TRUST INDONESIA",
          bank_code: "949"
        }, {
          bank_name: "BANK COMMONWEALTH",
          bank_code: "950"
        }
      ]);
    });
};
