
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('setting').del()
    .then(function () {
      // Inserts seed entries
      return knex('setting').insert([
        {
          st_keterangan: "Video Login Page",
          st_kode: "v_login",
          st_value: "tulus.mp4",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "string"
        }, {
          st_keterangan: "Template Email send Verification Register",
          st_kode: "t_register",
          st_value: "RegisterActivation.hbs",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "string"
        }, {
          st_keterangan: "Success Activation Register User",
          st_kode: "t_sregister",
          st_value: "SuccessActivation.hbs",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "string"
        }, {
          st_keterangan: "Website Link",
          st_kode: "l_link",
          st_value: "https://nenggala.id/webentry/",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "string"
        }, {
          st_keterangan: "PlayStore Link",
          st_kode: "apps_link",
          st_value: "https://play.google.com/store/apps/details?id=com.histore.indonesia",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "string"
        }, {
          st_keterangan: "VersionApps (Minima to Maksinal)",
          st_kode: "version_list",
          st_value: "[1,2,3,4]",
          st_id_admin: "1",
          st_isactive: "1",
          st_typevalue: "array"
        }
      ]);
    });
};
